﻿
using System.ComponentModel.DataAnnotations;

namespace RentAndTake.Models
{
    public class Credentials
    {
        [Required]
        //[EmailAddress]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password, ErrorMessage = "Password is not correct.")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,20}$", ErrorMessage = "Password can contain Alphanumeric and Special character ($@$!%*#?&) only.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public string MacAddress { get; set; }
        //[Required]
        [Display(Name = "ipAddress", Description = "IpAddress")]
        //[IPAddress(ErrorMessage = "IP address is not valid.")]
        public string IpAddress { get; set; }
        public string Browser { get; set; }
        public bool Active { get; set; }
        public string UserAgent { get; set; }
        public string Location { get; set; }
    }
}
