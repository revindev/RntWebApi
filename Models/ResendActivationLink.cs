﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentAndTake.Models
{
    public class ResendActivationLink
    {
        public string Useremail { get; set; }
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string Browser { get; set; }
        public string UserAgent { get; set; }
        public string Location { get; set; }
    }
}
