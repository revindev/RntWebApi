﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class SubCategoryEntryForm
    {
       
        public string Cid { get; set; }


        [Required]
        [MinLength(2)]
        [MaxLength(100)]
        [Display(Name = "Sub Category Name", Description = "Sub Category Name")]
        [RegularExpression(@"^[a-zA-Z\/ &-]+[a-zA-Z]+$", ErrorMessage = "Sub Category Name should contain Alphabets and (&,-,/) Only")]
        public string SubCategoryName { get; set; }
    }
}