﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class Cart
    {
        public int id { get; set; }
        public string PostID { get; set; }
        public string AdType { get; set; }
        public string SubAdType { get; set; }
        public DateTime AddToCartDate { get; set; }
        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
        public string BuyerID { get; set; }
        public string BuyerOrRenterName { get; set; }
        public string Price { get; set; }


        public string HoursOrDays { get; set; }
        public string ShipingDirection { get; set; }
        public string DeliveryPayment { get; set; }
        public string DeliveryMethods { get; set; }

        public decimal Total { get; set;}
        public decimal SubTotal { get; set;}

        //public int MasterOrder { get; set; }
        //public int ChildOrder { get; set; }
        //public int Line { get; set; }
        public DateTime ModifyCartDate { get; set; }       
              
      
       
        
    }
}