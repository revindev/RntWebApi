﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class AddressForm
    {
        [Required]
        [MinLength(1)]
        [MaxLength(40)]
        [Display(Name = "firstName", Description = "First Name")]
        [RegularExpression(@"^[A-Za-z]{1,100}$", ErrorMessage = "First name should contain only Alphabets.")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(40)]
        [Display(Name = "LastName", Description = "Last Name")]
        [RegularExpression(@"^[A-Za-z]{1,100}$", ErrorMessage = "First name should contain only Alphabets.")]
        public string LastName { get; set; }

        [Required]
        public string Address1 { get; set; }
        [Required]
        public string Address2 { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        [Display(Name = "City", Description = "City Name")]
        //[RegularExpression(@"/^[A-Za-z]{1,100}$/", ErrorMessage = "City name should contain only Alphabets.")]
        public string City { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        [Display(Name = "StateName", Description = "State Name")]
       // [RegularExpression(@"^[A-Za-z]{1,100}$", ErrorMessage = "State name should contain only Alphabets.")]
        public string State { get; set; }

        //  [RegularExpression(@"^[A-Za-z]{1,100}$", ErrorMessage = "State name should contain only Alphabets.")]
        [Required]
        public string Zip { get; set; }

        public string OwnerId { get; set; }

        public string DefaultAddress { get; set; }
    }
}