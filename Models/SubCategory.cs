﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class SubCategory
    {
        [Key]
        public Guid SCId { get; set; }

      
      
        public string Id { get; set; }
        public string Cid { get; set; }
        public string SubCategoryName { get; set; }
        public bool Status { get; set; }
        public DateTime Date_entered { get; set; }
        public DateTime Date_modified { get; set; }
    }
}