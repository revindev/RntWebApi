﻿namespace RentAndTake.Models
{
    public class EmailInformation
    {
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string Browser { set; get; }
        public string UserAgent { set; get; }
        public bool IsActive { get; set; }
        public string AccountNo { get; set; }
        public string EmailAddress { get; set; }
        public string Location { get; set; }
        public string UserName { get; set; }
        public string SuspendLink { get; set; }
        public string ContactLink { get; set; }
        public string WebLink { get; set; }
        public string ShieldLink { get; set; }
        public string ResetPassLink { get; set; }
        public string ActivationLink { get; set; }
        public string RemoveCCLink { get; set; }
        public string RemoveG2Factor { get; set; }

        public string Termsandcondition { get; set; }
        public string Telegram { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Medium { get; set; }
        public string Reddit { get; set; }
        public string Youtube { get; set; }
        public string Slack { get; set; }
        public string Steemit { get; set; }
        public string Email { get; set; }
        public string Attachement { get; set; }
        public string ContactNo { get; set; }

        public string FullName { get; set; }

        public string JobTitle { get; set; }

        public string FileName { get; set; }

        public string ContactNO { get; set; }
        public decimal Amount { get; set; }

        public string Message { get; set; }

    }
}
