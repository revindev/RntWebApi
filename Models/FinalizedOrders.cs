﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class FinalizedOrders
    {
        [key]
        public int id { get; set; }
        public string  MasterId { get; set; }
        public string ItemOrderId { get; set; }
        public DateTime Orderdate { get; set; }
        public string PostId { get; set; }

        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
        public string  BuyerId { get; set; }
        public string BuyerName { get; set; }

        public int SubAdType { get; set; }
        public decimal Price { get; set; }

        public string HoursOrDays { get; set; }
     
        public string DeliveryMethods { get; set; }
        public string ShipingDirection { get; set; }
        public string DeliveryPayment { get; set; }

        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }

        public string MessageToOwner { get; set; }
        public string ShippingAddress { get; set; }
        public string BillNumber { get; set; }
    }
}