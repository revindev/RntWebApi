﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentAndTake.Models
{
    public class LoginHistoryEntity
    {
        [Key]
        public Guid ID { get; set; }
        public string AccountNo { get; set; }
        public DateTime? LoginTime{ get; set; }
       
        public string Browser { get; set; }
        public string Location { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
    }
}
