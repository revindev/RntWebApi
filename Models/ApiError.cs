﻿
using Newtonsoft.Json;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace RentAndTake.Models
{
    public class ApiError
    {
        public ApiError(System.Web.Http.ModelBinding.ModelStateDictionary modelState)
        {
            Value = "Invalid parameters.";
            Detail = modelState
                .FirstOrDefault(x => x.Value.Errors.Any()).Value.Errors
                .FirstOrDefault().ErrorMessage;
        }

       

        public ApiError(ModelStateDictionary modelState)
        {
            Value = "Invalid parameters.";
            Detail = modelState
                .FirstOrDefault(x => x.Value.Errors.Any()).Value.Errors
                .FirstOrDefault().ErrorMessage;
        }

        public string Value { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Detail { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue("")]
        public string StackTrace { get; set; }
    }
}
