﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class Category
    {
        [Key]
        public Guid CId { get; set; }


        public string Id  { get; set; }
        public string CategoryName { get; set; }
        public bool Status { get; set; }
        public DateTime Date_entered { get; set; }
        public DateTime Date_modified { get; set; }
    }
}