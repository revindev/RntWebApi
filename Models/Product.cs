﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class Product
    {
        [Key]
        public Guid PId { get; set; }
       
      
        public int ProductNumber { get; set; }
        public string Id { get; set; }
        public string Owner_Id { get; set; }
        public bool Status { get; set; }
        public string Cat_Id { get; set; }
        public string Sub_Cat_Id { get; set; }
        public decimal Ship { get; set; }
        public string Condition { get; set; }
        public string Loc_City_State_Country { get; set; }
        public string Product_Name { get; set; }
        public string Display_Name { get; set; }
        public int Quantity_Available { get; set; }
        public string Product_Description { get; set; }
        public string Product_Note { get; set; }
        public string Model_Name { get; set; }
        public string Model_Number { get; set; }
        public decimal Rent_Amount { get; set; }
        public decimal Sale_Amount { get; set; }
        public int sale_Warranty { get; set; }
        public int Rent_Sale_Status { get; set; }
        public string rent_Negotiable { get; set; }
       // public string Sale_Negotiable { get; set; }
        public string Color { get; set; }
        public decimal Size_height { get; set; }
        public decimal Size_width { get; set; }
        public decimal Size_lenght { get; set; }
        public decimal weight { get; set; }

        public decimal height_units { get; set; }
        public decimal width_units  {  get; set;}
        public decimal length_units { get; set; }
        public decimal weight_units { get; set; }
        public String Photos { get; set; }

        public decimal rent_by_hour { get; set; }
        public decimal rent_by_day { get; set; }
        public String product_rating { get; set; }
        public String feedback { get; set; }
        public String alert_report { get; set; }
        public String ipaddress { get; set; }
        public String insurance_protection { get; set; }
        public DateTime date_entered { get; set; }
        public DateTime Date_modified { get; set; }

        public string  SearchTags { get; set; }

        public bool Sale_Negotiable { get; set; }
        public bool Rent_H_negotiable { get; set; }
        public bool Rent_D_negotiable { get; set; }
        public int Ad_type { get; set; }









    }
}