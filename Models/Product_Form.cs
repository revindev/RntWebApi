﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class Product_Form
    {



        public string Owner_Id { get; set; }
        [Required]      
        [Display(Name = "category", Description = "Plese Enter Category")]
        public string Cat_Id { get; set; }
                    

        public string Sub_Cat_Id { get; set; }

      

        [Required]
        public string Condition { get; set; }

        [Required]
        public string Loc_City_State_Country { get; set; }

        [Required]
       // [RegularExpression(@"/^[A-Za-z\d]*/", ErrorMessage = "First name should contain only Alphabets.")]
        public string Display_Name { get; set; }

        [Required]
       // [RegularExpression(@"/^[A-Za-z\d]*/", ErrorMessage = "Please Enter Only ")]
        public string Product_Description { get; set; }

        //  [Required]
        // [RegularExpression(@"^[0-9]([.,][0-9]{1,3})?$", ErrorMessage = "Please Enter Valid Amount")]



        public string Ad_type { get; set; }
        public String Photos { get; set; }

        // public String ipaddress { get; set; }
        [Required]
        public List<Product_Photos_Form> Images { get; set; }
        public List<string> SearchTags { get; set; }
      //  public string SearchTags { get; set; }


        [DefaultValue("0")]
        public string Sale_Amount { get; set; }
        [DefaultValue("0")]
        public string rent_by_hour { get; set; }
        [DefaultValue("0")]
        public string rent_by_day { get; set; }
        [DefaultValue("0")]
        public string Ship { get; set; }
        [DefaultValue(false)]
        public bool Sale_negotiable { get; set; }
        [DefaultValue(false)]
        public bool Rent_H_negotiable { get; set; }
        [DefaultValue(false)]
        public bool Rent_D_negotiable { get; set; }
        
    }



    

    public class Product_Photos_Form
    {
        // public string Photo_display_name { get; set; }
        // public string Photo_description { get; set; }
       // [Required]
        [Display(Name = "PhotoName", Description = "Photo Name")]
        public string Photo_file_name { get; set; }
        //  public bool Display_status { get; set; }

        [Required]
        [Display(Name = "Photo", Description = "Photo")]
        public string Photo { get; set; }

        [Required]
        [Display(Name = "PhotoExtension", Description = "Photo Extension")]
        public string PhotoExtension { get; set; }


    }
}