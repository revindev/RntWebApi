﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class CartFinalizeForm
    {
        public List<CartForm1> Items { get; set; }
    }
    public class CartForm1
    {
        public string PostID { get; set; }
        public string AdType { get; set; }
        public string SubAdType { get; set; }
        public string BuyerID { get; set; }
        public string OwnerId { get; set; }

        public decimal  price { get; set; }
        public string DeliveryType { get; set; }
        public string DeliveryPayment { get; set; }
        public string ShipingDirection { get; set; }
        public decimal Total { get; set; }
        public string HoursOrDays { get; set; }
        public decimal SubTotal { get; set; }
    }
}