﻿
using System.ComponentModel.DataAnnotations;

namespace RentAndTake.Models
{
    public class ResetPassword
    {
        [Required]
        [Display(Name = "useremail", Description = "User email")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", ErrorMessage = "Invalid Email Address")]
        public string Useremail { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(20)]
        [Display(Name = "userPassword", Description = "User password")]
        //[RegularExpression(@"^((?!\s).)*$", ErrorMessage = "Password can not contain space.")]
        [DataType(DataType.Password, ErrorMessage = "Password is not correct.")]
     
        public string UserPassword { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(20)]
        [Display(Name = "confirmPassword", Description = "Confirm password")]
       
        [Compare("UserPassword")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string UserToken { get; set; }
    }
}
