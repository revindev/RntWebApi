﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class ProductFilter
    {

       [DefaultValue("")]
        public string  Condition { get; set; }
        [DefaultValue("")]
        public string  Cat_Id { get; set; }
        [DefaultValue("")]
        public string  Sub_Cat_Id { get; set; }

        [DefaultValue(0)]
        public decimal MinPrice { get; set; }
        [DefaultValue(0)]
        public decimal MaxPrice { get; set; }
        [DefaultValue(0)]
        public int Ad_type { get; set; }

    }
}