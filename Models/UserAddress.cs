﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class UserAddress
    {
        [key]
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string OwnerId { get; set; }
        public string DefaultAddress { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime Modifydate { get; set; }
        public bool status { get; set; }
        internal void ForEach(Func<object, object> p)
        {
            throw new NotImplementedException();
        }
    }
}