﻿using System.Collections.Generic;
using System.ComponentModel;

namespace RentAndTake.Models
{
   
    public class ProductFilterNew
    {

        // [DefaultValue("")]
        //  public string  Condition { get; set; }
        [DefaultValue("")]
        public string Cat_Id { get; set; }
        [DefaultValue("")]
        public string Sub_Cat_Id { get; set; }

        [DefaultValue(0)]
        public decimal MinPrice { get; set; }
        [DefaultValue(0)]
        public decimal MaxPrice { get; set; }

        public List<string> Condition { get; set; }
        public List<int> AdTYpe   { get; set; }
         //public List<Colour> AvailableColours { get; set; }




}

    public class Conditions_Filter
    {
        [DefaultValue("0")]
        public string Condition { get; set; }
    }
    public class Ad_type_Filter
    {
        [DefaultValue("0")]
        public string Adtype { get; set; }
    }
}