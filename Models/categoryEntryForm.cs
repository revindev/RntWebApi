﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class CategoryEntryForm
    {
        [Required]
        [MinLength(2)]
        [MaxLength(100)]
        [Display(Name = "Category Name", Description = "Category Name")]
        [RegularExpression(@"^[a-zA-Z\/ &-]+[a-zA-Z]+$", ErrorMessage = "Category Name should contain Alphabets and (&,-,/) Only")]
        public string CategoryName { get; set; }


    }
}