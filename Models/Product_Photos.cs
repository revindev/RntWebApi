﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class Product_Photos
    {
        [Key]
        public Guid PhotoId { get; set; }
   
        public string Pid { get; set; }
        public string Photo_display_name { get; set; }
        public string Photo_description { get; set; }
        public string Photo_file_name { get; set; }
        public bool Display_status { get; set; }
        public DateTime Date_entered { get; set; }
        public string Photo { get; set; }
        public string PhotoExtension { get; set; }
       


    }
}