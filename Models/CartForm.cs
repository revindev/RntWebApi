﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class CartForm
    {
        [Required]
        public string PostID { get; set; }

        public string AdType { get; set; }
        public string SubAdType { get; set; }
        [Required]
        public string BuyerID { get; set; }
        [Required]
        public string OwnerId { get; set; }

    }
}