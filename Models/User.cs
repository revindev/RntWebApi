﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace RentAndTake.Models
{
    public class User
    {
        [Required]
      
        [MaxLength(40)]
        [Display(Name = "emailID", Description = "Email")]
        //[EmailAddress(ErrorMessage = "Invalid Email Address")]
        [RegularExpression(@"^[a-z0-9_]+\.?[a-z0-9_]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?[.][a-z]{2,61}(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$", ErrorMessage = "Invalid Email Address")]
        public string EmailID { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(20)]
        [Display(Name = "password", Description = "Password")]
        [DataType(DataType.Password, ErrorMessage = "Password is not correct.")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,20}$", ErrorMessage = "Password can contain Alphanumeric and Special character ($@$!%*#?&) only.")]
        public String Password { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(20)]
        [Display(Name = "confirmPassword", Description = "Confirm password")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

       

        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        [Display(Name = "firstName", Description = "First Name")]
        [RegularExpression(@"^[A-Za-z]{1,100}$", ErrorMessage = "First name should contain only Alphabets.")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        [Display(Name = "lastName", Description = "Last Name")]
        [RegularExpression(@"^[A-Za-z]{1,100}$", ErrorMessage = "Last name should contain only Alphabets.")]
        public string LastName { get; set; }

     

        //[Required]
        //[Display(Name = "dob", Description = "DOB")]
        //[Date(-100, -3, ErrorMessage = "Value for {0} must be before 3 years from today.")]
        //public DateTime? DOB { get; set; }


        [Required]
        [Display(Name = "Birthday", Description = "Date of Birth")]
        
        public int Birthday { get; set; }

        [Required]
        [Display(Name = "Birthmonth", Description = "Month of Birth")]
       
        public int Birthmonth { get; set; }

        [Required]
        [Display(Name = "Birthyear", Description = "Year of Birth")]
    
        public int Birthyear { get; set; }



        [Display(Name = "location", Description = "Location")]
        public string Location { get; set; }

        [Display(Name = "macAddress", Description = "MacAddress")]
        public string MacAddress { get; set; }

        [Display(Name = "ipAddress", Description = "IpAddress")]
        
        public string IpAddress { get; set; }

        [Display(Name = "browser", Description = "Browser")]
        public string Browser { get; set; }

     
    }

    public enum ChildNode
    {
        LeftChild = 0,
        RightChild = 1
    }

}