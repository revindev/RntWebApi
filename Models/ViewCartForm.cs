﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentAndTake.Models
{
    public class ViewCartForm
    {
        public string PostID { get; set; }
        public string SubAdType { get; set; }
        public string BuyerID { get; set; }

        public List<ProductDetails> ProductDetails { get; set; }

    }

    public class ProductDetails
    {
        public string Owner_Id { get; set; }       
        public string Cat_Id { get; set; }
        public string Sub_Cat_Id { get; set; }
        public string Condition { get; set; }
        public string Loc_City_State_Country { get; set; }
        public string Display_Name { get; set; }
        public string Product_Description { get; set; }
        public string Ad_type { get; set; }
        public String Photos { get; set; }      
      
        public string Sale_Amount { get; set; }
        public string rent_by_hour { get; set; }
        public string rent_by_day { get; set; }
        public string Ship { get; set; }
        public bool Sale_negotiable { get; set; }
        public bool Rent_H_negotiable { get; set; }
        public bool Rent_D_negotiable { get; set; }


    }

}