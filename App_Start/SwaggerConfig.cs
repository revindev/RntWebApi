using System.Web.Http;
using WebActivatorEx;
using RentAndTake;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace RentAndTake
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
    .EnableSwagger(c => c.SingleApiVersion("v1", "Rent And Take WebApi"))
     .EnableSwaggerUi(c =>
     {
         // Use the "InjectJavaScript" option to invoke one or more custom JavaScripts after the swagger-ui
         // has loaded. The file must be included in your project as an "Embedded Resource", and then the resource's
         // "Logical Name" is passed to the method as shown above.
         c.InjectJavaScript(thisAssembly, "RentAndTake.Scripts.CustomSwagger.js");
     });
        }
    }
}
