using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RentAndTake.Context;
using RentAndTake.Controllers;
using RentAndTake.Models;
using RentAndTake.Services;
using System.Data.Entity;
using System.Web.Http;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Unity.WebApi;

namespace RentAndTake
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IUserService, DefaultUserService>();
            container.RegisterType<IPostingAddService, PostingAddService>();
          
            container.RegisterType<IMasterService, MasterService>();
            container.RegisterType<IcartService, CartService>();



            container.RegisterType<ICommonService, CommonService>();
            container.RegisterType<DbContext, RnTContext>(new HierarchicalLifetimeManager());
            container.RegisterType<UserManager<UserEntity>>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<UserEntity>, UserStore<UserEntity>>(new HierarchicalLifetimeManager());

          //  container.RegisterType<IPostingAddService<Product>>(new HierarchicalLifetimeManager());

            //container.RegisterType<IPostingAddService<Product>, Product<Product>>(new HierarchicalLifetimeManager());


            // container.RegisterType<AccountController>(new InjectionConstructor());
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}