﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using RentAndTake.Models;
using RentAndTake.Services;

namespace RentAndTake.Controllers
{
    public class OrderController : ApiController
    {
        // GET api/<controller>
        private IPostingAddService _postingAddService;
        private IcartService _cartService;
        private IMasterService _masterService;

        public OrderController(IcartService cartService, IMasterService masterService)
        {
            _cartService = cartService;
            _masterService = masterService;
        }


        [HttpPost]
       
        public IHttpActionResult AddTocart(CartForm cartForm)
        {
            if (ModelState.IsValid)
            {
                // var identity = (ClaimsIdentity)User.Identity;
                // IEnumerable<Claim> claims = identity.Claims;
                //  var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;

                //  var (result, succeeded) = _postingAddService.CreateAdd(product_Form, prinicpal).GetAwaiter().GetResult();
                var (result, succeeded) = _cartService.AddToCart(cartForm).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        // AddToCartMultipleItem

        [HttpPost]
        public IHttpActionResult AddToCartMultipleItem(CartFinalizeForm cartFinalizeForm)
        {
            if (ModelState.IsValid)
            {
                // var identity = (ClaimsIdentity)User.Identity;
                // IEnumerable<Claim> claims = identity.Claims;
                //  var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;

                //  var (result, succeeded) = _postingAddService.CreateAdd(product_Form, prinicpal).GetAwaiter().GetResult();
                var (result, succeeded) = _cartService.AddToCartMultipleItem(cartFinalizeForm).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpGet]
        public IHttpActionResult ViewCart(String Bid)
        {
            var (result, succeeded) = _cartService.GetCartByBuyerId(Bid).GetAwaiter().GetResult();
            if (succeeded)
            {
                return Ok(JsonConvert.DeserializeObject(result));
            }
            else
            {
                return BadRequest(result);
            }
        }

        //DeletItemIntheCart

        [HttpPost]

        public IHttpActionResult DeleteFormCart(string BuyerID,String PostId="" )
        {
            if (ModelState.IsValid)
            {
               var (result, succeeded) = _cartService.DeletCart(PostId, BuyerID).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpPost]
        public IHttpActionResult PlaceOrder(OrderForm orderForm)
        {
            if (ModelState.IsValid)
            {
                
                
                    var (result, succeeded) = _cartService.PlaceOrderFinal(orderForm).GetAwaiter().GetResult();
                    if (succeeded)
                        return Ok(JsonConvert.DeserializeObject(result));
                    else
                        return BadRequest(result);
                
               
            }
            else
            {
                return BadRequest(ModelState);
            }
        }





    }
}