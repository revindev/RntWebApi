﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using RentAndTake.Models;
using RentAndTake.Services;

namespace RentAndTake.Controllers
{
    public class AddPostController : ApiController
    {
        private IPostingAddService _postingAddService;
        private IMasterService _masterService;

        public AddPostController(IPostingAddService postingAddService, IMasterService masterService)
        {
            _postingAddService = postingAddService;
            _masterService = masterService;
        }



        [HttpPost]
        [Authorize]
        public IHttpActionResult PostingAdd(Product_Form product_Form)
        {
            if (ModelState.IsValid)
            {
                // var identity = (ClaimsIdentity)User.Identity;
                // IEnumerable<Claim> claims = identity.Claims;
                var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;

                var (result, succeeded) = _postingAddService.CreateAdd(product_Form, prinicpal).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        public IHttpActionResult GetPosts(String CatId="", String SubCatId = "")
        {
            var (result, succeeded) = _postingAddService.GetPost(CatId, SubCatId).GetAwaiter().GetResult();
           // var (result, succeeded) = _postingAddService.(CatId, SubCatId).GetAwaiter().GetResult();

            if (succeeded)
            {               
                return Ok(JsonConvert.DeserializeObject(result));              
            }
            else
            {
                return BadRequest(result);
            }
        }

      //  [HttpGet]
      //[HttpGet]
      //  public IHttpActionResult GetPostWithFilter([FromUri] ProductFilter productFilter)
      //  {
      //      var (result, succeeded) = _postingAddService.GetPostByFilters(productFilter).GetAwaiter().GetResult();
      //      if (succeeded)
      //          return Ok(JsonConvert.DeserializeObject(result));
      //      else
      //          return BadRequest(result);

      //  }


        //  [HttpGet]
        //[HttpPost]
        //public IHttpActionResult GetPostWithFilterOld(ProductFilter productFilter)
        //{
        //    var (result, succeeded) = _postingAddService.GetPostByFilters(productFilter);
        //    if (succeeded)
        //        return Ok(JsonConvert.DeserializeObject(result));
        //    else
        //        return BadRequest(result);

        //}

        //ProductFilterNew

        [HttpPost]
        public IHttpActionResult GetPostWithFilter(ProductFilterNew productFilterNew)
        {
            var (result, succeeded) = _postingAddService.GetPostByFilters(productFilterNew);
            if (succeeded)
                return Ok(JsonConvert.DeserializeObject(result));
            else
                return BadRequest(result);

        }



        // GET api/<controller>/5

        [HttpGet]
        [Authorize]
        public IHttpActionResult GetAllcategory()
        {
            var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var (result, succeeded) = _masterService.GetAllCategory(prinicpal).GetAwaiter().GetResult();
            // var (result, succeeded, error) = _masterService.GetAllCategory(prinicpal);
            if (succeeded) return Ok((result));
            return BadRequest(result);

            // return BadRequest(new JsonResult(error));
        }


        [HttpGet]
       [Authorize]
        public IHttpActionResult GetSubcategoryByCatId(string Cid)
        {
            var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var (result, succeeded) = _masterService.GetSubCategory(Cid, prinicpal).GetAwaiter().GetResult();
            // var (result, succeeded, error) = _masterService.GetAllCategory(prinicpal);
            if (succeeded) return Ok((result));
            return BadRequest(result);

            // return BadRequest(new JsonResult(error));
        }

        [HttpPost]
        public IHttpActionResult Search(String SearchText="", string catId = "", String subCatId = "")
        {
            var (result, succeeded) = _postingAddService.GetPostSearchWithcatAndSubCat(SearchText, catId, subCatId).GetAwaiter().GetResult(); ;
           // var (result, succeeded) = _postingAddService.GetPostSearch(SearchText).GetAwaiter().GetResult(); ;

            if (succeeded)
                return Ok(JsonConvert.DeserializeObject(result));
            else
                return BadRequest(result);

        }


        // GetPostByProductId

        [HttpGet]
        public IHttpActionResult GetPostByProductId(String ProductId)
        {
            var (result, succeeded) = _postingAddService.GetPostByProductId(ProductId).GetAwaiter().GetResult();
            if (succeeded)
            {
                return Ok(JsonConvert.DeserializeObject(result));
            }
            else
            {
                return BadRequest(result);
            }
        }


    }
}