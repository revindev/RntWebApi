﻿using System.Threading;
using RentAndTake.Models;
using System.Web.Http;

using RentAndTake.Services;
//using System.Net.Http;

using Newtonsoft.Json;
using System;

namespace RentAndTake.Controllers
{
   // [Route("api/[controller]")]
    public class AccountController : ApiController
    {
        private IUserService _userService;


        public AccountController(IUserService userService)
        {
            _userService= userService;
        }
        #region Signup ANd Login
        //[System.Web.Http.HttpPost]
        [HttpPost]
        public IHttpActionResult SignIn([FromBody] Credentials Credentials)
        {
            if (ModelState.IsValid)
            {

               // Credentials.IpAddress = GetIPAddress();
              

                var (result, succeeded) = _userService.SignIn(Credentials).GetAwaiter().GetResult();
                if (succeeded)
                {
                     return  Ok(JsonConvert.DeserializeObject(result)); 
                  
                }
                else
                {
                    return BadRequest(result);
                    //return Ok(JsonConvert.DeserializeObject(result));                
                }
            }
            else
            {

                return BadRequest(ModelState);
               
            }
        }


        [HttpPost]
        public IHttpActionResult Register(User register)
        {

            if (ModelState.IsValid)
            {
                register.IpAddress = GetIPAddress();
                var (result, succeeded) = _userService.CreateUserAsync(register).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);
            }
            else
            {

                return BadRequest(ModelState);

               

            }
            

        }





        #endregion
        //   AddAddress
        
        #region Address Realted Controllers 
        [HttpPost]

        public IHttpActionResult AddShippingAddress(AddressForm addressForm)
        {
            if (ModelState.IsValid)
            {
                // var identity = (ClaimsIdentity)User.Identity;
                // IEnumerable<Claim> claims = identity.Claims;
                //  var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;

                //  var (result, succeeded) = _postingAddService.CreateAdd(product_Form, prinicpal).GetAwaiter().GetResult();
                var (result, succeeded) = _userService.AddAddress(addressForm).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAllAddressByAddressId(String OwnerId,string AddressId)
        {
            var (result, succeeded) = _userService.GetAddressByAddressId(OwnerId, AddressId).GetAwaiter().GetResult();
            if (succeeded)
            {
                return Ok(JsonConvert.DeserializeObject(result));
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAllAddressByOwnerId(String Bid)
        {
            var (result, succeeded) = _userService.GetAddressByOwnerId(Bid).GetAwaiter().GetResult();
            if (succeeded)
            {
                return Ok(JsonConvert.DeserializeObject(result));
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        public IHttpActionResult SetDefaultAddressByOwnerId(String OwnerOrBuyerId, string AddressId)
        {
            var (result, succeeded) = _userService.SetDefaultAddress(OwnerOrBuyerId, AddressId).GetAwaiter().GetResult();
            if (succeeded)
            {
                return Ok(JsonConvert.DeserializeObject(result));
            }
            else
            {
                return BadRequest(result);
            }
        }


        [HttpPost]

        public IHttpActionResult UpdateAddress(string AddressId, AddressForm addressForm)
        {
            if (ModelState.IsValid)
            {
                // var identity = (ClaimsIdentity)User.Identity;
                // IEnumerable<Claim> claims = identity.Claims;
                //  var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;

                //  var (result, succeeded) = _postingAddService.CreateAdd(product_Form, prinicpal).GetAwaiter().GetResult();
                var (result, succeeded) = _userService.UpdateAddress(AddressId, addressForm).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost]

        public IHttpActionResult DeleteAddress(string OwnerId, string AddressId)
        {
            if (ModelState.IsValid)
            {
                // var identity = (ClaimsIdentity)User.Identity;
                // IEnumerable<Claim> claims = identity.Claims;
                //  var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;

                //  var (result, succeeded) = _postingAddService.CreateAdd(product_Form, prinicpal).GetAwaiter().GetResult();
                var (result, succeeded) = _userService.DeleteAddress(OwnerId, AddressId).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        #endregion


        #region Common Methods and controllers  
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {  
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }


        [HttpPost]
        public IHttpActionResult activateuser(string code)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("server error");
            }
            else
            {
                // var (result, succeeded) = _userService.ActivateUserLogin(code);
                var (result, succeeded) = _userService.ActivateUserLogin(code).GetAwaiter().GetResult();
                if (succeeded) return Ok(JsonConvert.DeserializeObject(result));
                else //return Ok(JsonConvert.DeserializeObject(result));
                    return BadRequest(result);
            }
        }

        [HttpPost]
        public IHttpActionResult ForgetPassowrd(string code)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("server error");
            }
            else
            {
                // var (result, succeeded) = _userService.ActivateUserLogin(code);
                var (result, succeeded) = _userService.ForgetPassLink(code).GetAwaiter().GetResult();
                if (succeeded) return Ok(JsonConvert.DeserializeObject(result));
                else //return Ok(JsonConvert.DeserializeObject(result));
                    return BadRequest(result);
            }
        }

        [HttpPost]
        public IHttpActionResult VerifyForgetPassLink(ResetPassword resetPassword)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("server error");
            }
            else
            {
                // var (result, succeeded) = _userService.ActivateUserLogin(code);
                var (result, succeeded) = _userService.SetForgetPass(resetPassword).GetAwaiter().GetResult();
                if (succeeded) return Ok(JsonConvert.DeserializeObject(result));
                else //return Ok(JsonConvert.DeserializeObject(result));
                    return BadRequest(result);
            }
        }

        #endregion




    }


}