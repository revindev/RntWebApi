﻿using Newtonsoft.Json;
using RentAndTake.Models;
using RentAndTake.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;

namespace RentAndTake
{
    public class MasterController : ApiController
    {

     
        private IMasterService _masterService;

        public MasterController( IMasterService masterService)
        {
            _masterService = masterService;
        }

        [HttpPost]
        [Authorize]
        public IHttpActionResult AddCategory(CategoryEntryForm categoryEntryForm)
        {

            if (ModelState.IsValid)
            {
                // var identity = (ClaimsIdentity)User.Identity;
                // IEnumerable<Claim> claims = identity.Claims;
                var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;

                var (result, succeeded) = _masterService.AddCategory(categoryEntryForm, prinicpal).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);

            }
            else
            {
                return BadRequest(ModelState);

            }


        }

        [HttpPost]
        [Authorize]
        public IHttpActionResult AddSubCategory(SubCategoryEntryForm subCategoryEntryForm)
        {

            if (ModelState.IsValid)
            {
                
                var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;

                var (result, succeeded) = _masterService.AddSubCategory(subCategoryEntryForm, prinicpal).GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);

            }
            else
            {
                return BadRequest(ModelState);

            }


        }


        [HttpPost]
        public IHttpActionResult GetNavBar()
        {
                var (result, succeeded) = _masterService.GetNavBar().GetAwaiter().GetResult();
                if (succeeded)
                    return Ok(JsonConvert.DeserializeObject(result));
                else
                    return BadRequest(result);

        }






    }
}