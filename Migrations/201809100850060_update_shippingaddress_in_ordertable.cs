namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_shippingaddress_in_ordertable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FinalizedOrders", "ShippingAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FinalizedOrders", "ShippingAddress");
        }
    }
}
