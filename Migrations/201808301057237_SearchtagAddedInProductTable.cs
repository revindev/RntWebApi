namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SearchtagAddedInProductTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "SearchTags", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "SearchTags");
        }
    }
}
