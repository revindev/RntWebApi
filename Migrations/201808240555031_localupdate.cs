namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class localupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Rent_H_negotiable", c => c.Boolean(nullable: true));
            AddColumn("dbo.Products", "Rent_D_negotiable", c => c.Boolean(nullable: true));
            AlterColumn("dbo.Products", "Sale_negotiable", c => c.Boolean(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Sale_negotiable", c => c.String());
            DropColumn("dbo.Products", "Rent_D_negotiable");
            DropColumn("dbo.Products", "Rent_H_negotiable");
        }
    }
}
