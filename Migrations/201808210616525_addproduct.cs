namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addproduct : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Product_Photos",
                c => new
                    {
                        PhotoId = c.Guid(nullable: false),
                        Pid = c.String(),
                        Photo_display_name = c.String(),
                        Photo_description = c.String(),
                        Photo_file_name = c.String(),
                        Display_status = c.Boolean(nullable: false),
                        Date_entered = c.DateTime(nullable: false),
                        Photo = c.String(),
                    })
                .PrimaryKey(t => t.PhotoId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        PId = c.Guid(nullable: false),
                        ProductNumber = c.String(),
                        Owner_Id = c.String(),
                        Status = c.Boolean(nullable: false),
                        Cat_Id = c.String(),
                        Condition = c.String(),
                        Loc_City_State_Country = c.String(),
                        Product_Name = c.String(),
                        Display_Name = c.String(),
                        Quantity_Available = c.Int(nullable: false),
                        Product_Description = c.String(),
                        Product_Note = c.String(),
                        Model_Name = c.String(),
                        Model_Number = c.String(),
                        Rent_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sale_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        sale_Warranty = c.Int(nullable: false),
                        Rent_Sale_Status = c.Int(nullable: false),
                        rent_Negotiable = c.String(),
                        Sale_Negotiable = c.String(),
                        Color = c.String(),
                        Size_height = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Size_width = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Size_lenght = c.Decimal(nullable: false, precision: 18, scale: 2),
                        weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        height_units = c.Decimal(nullable: false, precision: 18, scale: 2),
                        width_units = c.Decimal(nullable: false, precision: 18, scale: 2),
                        length_units = c.Decimal(nullable: false, precision: 18, scale: 2),
                        weight_units = c.Decimal(nullable: false, precision: 18, scale: 2),
                        photos = c.String(),
                        rent_by_hour = c.Decimal(nullable: false, precision: 18, scale: 2),
                        rent_by_day = c.Decimal(nullable: false, precision: 18, scale: 2),
                        product_rating = c.String(),
                        feedback = c.String(),
                        alert_report = c.String(),
                        ipaddress = c.String(),
                        insurance_protection = c.String(),
                        date_entered = c.DateTime(nullable: false),
                        Date_modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Products");
            DropTable("dbo.Product_Photos");
        }
    }
}
