namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ADD_parameter_AdType_inProductTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Ad_type", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Ad_type");
        }
    }
}
