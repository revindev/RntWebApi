// <auto-generated />
namespace RentAndTake.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SearchtagAddedInProductTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SearchtagAddedInProductTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201808301057237_SearchtagAddedInProductTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
