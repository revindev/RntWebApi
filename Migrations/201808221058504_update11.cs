namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update11 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "ProductNumber", c => c.Int(nullable: false, identity: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "ProductNumber", c => c.String(nullable: false));
        }
    }
}
