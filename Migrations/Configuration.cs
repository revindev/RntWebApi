namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RentAndTake.Context.RnTContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RentAndTake.Context.RnTContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.


            //context.categories.AddOrUpdate(x => x.Id,
            //  new Models.Category(){ CId = new Guid(), CategoryName = "Appliances", Date_entered = DateTime.UtcNow, Status = true},
            //  new Models.Category() { CId = new Guid(), CategoryName = "Arts & Crafts", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Auto Parts", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Baby & Kids", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Beauty Care", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Bicycles", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Boats", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Books", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Cars & Trucks", Date_entered = DateTime.UtcNow, Status = true },

            //  new Models.Category() { CId = new Guid(), CategoryName = "CDs, DVDs & Blu Ray", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Cell Phones", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Clothing", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Computers", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Electronics", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Furniture", Date_entered = DateTime.UtcNow, Status = true },

            //  new Models.Category() { CId = new Guid(), CategoryName = "Home & Garden", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Jewelry", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Kitchen", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.Category() { CId = new Guid(), CategoryName = "Motocycles", Date_entered = DateTime.UtcNow, Status = true }
              
            //  );

            //context.SubCategories.AddOrUpdate(x => x.Id,
            //  new Models.SubCategory() { SCId = new Guid(),Cid=1, SubCategoryName = "Appliances", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Arts & Crafts", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Auto Parts", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Baby & Kids", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Beauty Care", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Bicycles", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Boats", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Books", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Cars & Trucks", Date_entered = DateTime.UtcNow, Status = true },

            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "CDs, DVDs & Blu Ray", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Cell Phones", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Clothing", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Computers", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Electronics", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Furniture", Date_entered = DateTime.UtcNow, Status = true },

            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Home & Garden", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Jewelry", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Kitchen", Date_entered = DateTime.UtcNow, Status = true },
            //  new Models.SubCategory() { SCId = new Guid(), Cid = 1, SubCategoryName = "Motocycles", Date_entered = DateTime.UtcNow, Status = true }


            //  );
            
            
        }
            
        
    }
}
