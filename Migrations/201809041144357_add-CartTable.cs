namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCartTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        MasterOrder = c.Int(nullable: false),
                        ChildOrder = c.Int(nullable: false),
                        Line = c.Int(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        PostID = c.String(),
                        OwnerId = c.String(),
                        OwnerName = c.String(),
                        BuyerID = c.String(),
                        BuyerOrRenterName = c.String(),
                        AdType = c.String(),
                        Price = c.String(),
                        HoursOrDays = c.String(),
                        DeliveryMethods = c.String(),
                        ShipingDirection = c.String(),
                        DeliveryPayment = c.String(),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SubTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Carts");
        }
    }
}
