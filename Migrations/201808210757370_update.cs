namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product_Photos", "PhotoExtension", c => c.String());
            AddColumn("dbo.Products", "Sub_Cat_Id", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "Ship", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Products", "Cat_Id", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Cat_Id", c => c.String());
            DropColumn("dbo.Products", "Ship");
            DropColumn("dbo.Products", "Sub_Cat_Id");
            DropColumn("dbo.Product_Photos", "PhotoExtension");
        }
    }
}
