namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDataTypeOfaADTYPE_fromString_INt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "Ad_type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Ad_type", c => c.String());
        }
    }
}
