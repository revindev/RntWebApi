namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "ProductNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "Sale_negotiable", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Products", "Sale_Negotiable", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Sale_Negotiable", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Products", "Sale_negotiable", c => c.String());
            AlterColumn("dbo.Products", "ProductNumber", c => c.Int(nullable: false, identity: true));
        }
    }
}
