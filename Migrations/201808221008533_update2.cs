namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Products", "ProductNumber", c => c.String(nullable: false));
            AlterColumn("dbo.SubCategories", "Id", c => c.Int(nullable: false, identity: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SubCategories", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "ProductNumber", c => c.String());
            AlterColumn("dbo.Categories", "Id", c => c.Int(nullable: false));
        }
    }
}
