namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CId = c.Guid(nullable: false),
                        Id = c.Int(nullable: false),
                        CategoryName = c.String(),
                        Status = c.Boolean(nullable: false),
                        Date_entered = c.DateTime(nullable: false),
                        Date_modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CId);
            
            CreateTable(
                "dbo.SubCategories",
                c => new
                    {
                        SCId = c.Guid(nullable: false),
                        Id = c.Int(nullable: false),
                        Cid = c.Int(nullable: false),
                        SubCategoryName = c.String(),
                        Status = c.Boolean(nullable: false),
                        Date_entered = c.DateTime(nullable: false),
                        Date_modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SCId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SubCategories");
            DropTable("dbo.Categories");
        }
    }
}
