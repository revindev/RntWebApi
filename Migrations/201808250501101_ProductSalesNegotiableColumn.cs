namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductSalesNegotiableColumn : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "Sale_Negotiable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Sale_Negotiable", c => c.String());
        }
    }
}
