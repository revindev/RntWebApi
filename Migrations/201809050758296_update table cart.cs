namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetablecart : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Carts", "SubAdType", c => c.String());
            AddColumn("dbo.Carts", "AddToCartDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Carts", "AddToCartDate");
            DropColumn("dbo.Carts", "SubAdType");
        }
    }
}
