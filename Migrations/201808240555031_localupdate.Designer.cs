// <auto-generated />
namespace RentAndTake.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class localupdate : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(localupdate));
        
        string IMigrationMetadata.Id
        {
            get { return "201808240555031_localupdate"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
