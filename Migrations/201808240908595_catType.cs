namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class catType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "Id", c => c.String(nullable: false));
            AlterColumn("dbo.Products", "Cat_Id", c => c.String());
            AlterColumn("dbo.Products", "Sub_Cat_Id", c => c.String());
            AlterColumn("dbo.Products", "Sale_negotiable", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Products", "Sale_Negotiable", c => c.String());
            AlterColumn("dbo.SubCategories", "Id", c => c.String(nullable: false));
            AlterColumn("dbo.SubCategories", "Cid", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SubCategories", "Cid", c => c.Int(nullable: false));
            AlterColumn("dbo.SubCategories", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Products", "Sale_Negotiable", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Products", "Sale_negotiable", c => c.String());
            AlterColumn("dbo.Products", "Sub_Cat_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "Cat_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Categories", "Id", c => c.Int(nullable: false, identity: true));
        }
    }
}
