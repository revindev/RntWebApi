namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Id", c => c.String());
            AlterColumn("dbo.Products", "Sale_negotiable", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Products", "Sale_Negotiable", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Sale_Negotiable", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Products", "Sale_negotiable", c => c.String());
            DropColumn("dbo.Products", "Id");
        }
    }
}
