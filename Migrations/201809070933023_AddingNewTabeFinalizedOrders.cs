namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingNewTabeFinalizedOrders : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FinalizedOrders",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        MasterId = c.String(),
                        ItemOrderId = c.String(),
                        Orderdate = c.DateTime(nullable: false),
                        PostId = c.String(),
                        OwnerId = c.String(),
                        OwnerName = c.String(),
                        BuyerId = c.String(),
                        BuyerName = c.String(),
                        SubAdType = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HoursOrDays = c.String(),
                        DeliveryMethods = c.String(),
                        ShipingDirection = c.String(),
                        DeliveryPayment = c.String(),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SubTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MessageToOwner = c.String(),
                        BillNumber = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FinalizedOrders");
        }
    }
}
