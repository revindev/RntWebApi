namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateTableUserAdressAddStatusColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserAddresses", "status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserAddresses", "status");
        }
    }
}
