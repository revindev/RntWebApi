namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changesincarttable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Carts", "ModifyCartDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Carts", "MasterOrder");
            DropColumn("dbo.Carts", "ChildOrder");
            DropColumn("dbo.Carts", "Line");
            DropColumn("dbo.Carts", "OrderDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Carts", "OrderDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Carts", "Line", c => c.Int(nullable: false));
            AddColumn("dbo.Carts", "ChildOrder", c => c.Int(nullable: false));
            AddColumn("dbo.Carts", "MasterOrder", c => c.Int(nullable: false));
            DropColumn("dbo.Carts", "ModifyCartDate");
        }
    }
}
