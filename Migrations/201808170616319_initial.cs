namespace RentAndTake.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailTemplates",
                c => new
                    {
                        TemplateCode = c.String(nullable: false, maxLength: 128),
                        TemplateType = c.String(),
                        TemplateName = c.String(),
                        SMTPUsername = c.String(),
                        SenderEmail = c.String(),
                        SenderName = c.String(),
                        SMTPPassword = c.String(),
                        Subject = c.String(),
                        Body = c.String(),
                        HostName = c.String(),
                        HostPort = c.Int(nullable: false),
                        EnableSSL = c.Boolean(nullable: false),
                        TimeOut = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TemplateCode);
            
            CreateTable(
                "dbo.GenericSettings",
                c => new
                    {
                        SettingID = c.Guid(nullable: false),
                        AdminAccountId = c.String(nullable: false, maxLength: 128),
                        SettingName = c.String(nullable: false, maxLength: 128),
                        SubSettingName = c.String(nullable: false, maxLength: 128),
                        DefaultTextValue20_1 = c.String(maxLength: 20),
                        DefaultTextValue20_2 = c.String(maxLength: 20),
                        DefaultTextValue50_1 = c.String(maxLength: 50),
                        DefaultTextValue50_2 = c.String(maxLength: 50),
                        DefaultTextValue100_1 = c.String(maxLength: 100),
                        DefaultTextValue100_2 = c.String(maxLength: 100),
                        DefaultTextValue250_1 = c.String(maxLength: 250),
                        DefaultTextValue250_2 = c.String(maxLength: 250),
                        DefaultTextMax = c.String(),
                        DefaultTextMax1 = c.String(),
                        DefaultTextMax2 = c.String(),
                        DefaultTextMax3 = c.String(),
                        DefaultTextMax4 = c.String(),
                        DefalutInteger1 = c.Int(nullable: false),
                        DefalutInteger2 = c.Int(nullable: false),
                        DefalutInteger3 = c.Int(nullable: false),
                        DefalutInteger4 = c.Int(nullable: false),
                        DefalutInteger5 = c.Int(nullable: false),
                        DefaultDecimal1 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DefaultDecimal2 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DefaultDecimal3 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DefaultDecimal4 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DefaultDecimal5 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DefaultDateTime1 = c.DateTime(),
                        DefaultDateTime2 = c.DateTime(),
                        DefaultDateTime3 = c.DateTime(),
                        DefaultDateTime4 = c.DateTime(),
                        DefaultDateTime5 = c.DateTime(),
                        DefaultBool1 = c.Boolean(nullable: false),
                        DefaultBool2 = c.Boolean(nullable: false),
                        DefaultBool3 = c.Boolean(nullable: false),
                        DefaultBool4 = c.Boolean(nullable: false),
                        DefaultBool5 = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.SettingID, t.AdminAccountId, t.SettingName, t.SubSettingName });
            
            CreateTable(
                "dbo.LoginHistoryEntities",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        AccountNo = c.String(),
                        LoginTime = c.DateTime(),
                        Browser = c.String(),
                        Location = c.String(),
                        IpAddress = c.String(),
                        MacAddress = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Birthday = c.Int(nullable: false),
                        Birthmonth = c.Int(nullable: false),
                        Birthyear = c.Int(nullable: false),
                        Location = c.String(),
                        MacAddress = c.String(),
                        IpAddress = c.String(),
                        Browser = c.String(),
                        Status = c.Boolean(nullable: false),
                        Date_entered = c.DateTimeOffset(precision: 7),
                        Date_modified = c.DateTimeOffset(precision: 7),
                        Alias = c.String(),
                        Member_status = c.String(),
                        Member_value = c.String(),
                        Member_since = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Zip_a = c.String(),
                        Zip_b = c.String(),
                        Country = c.String(),
                        Email2 = c.String(),
                        u = c.String(),
                        Pq1 = c.String(),
                        Pq2 = c.String(),
                        Pq3 = c.String(),
                        Pq4 = c.String(),
                        Pq5 = c.String(),
                        Pa1 = c.String(),
                        Pa2 = c.String(),
                        Pa3 = c.String(),
                        Pa4 = c.String(),
                        Pa5 = c.String(),
                        HPha = c.String(),
                        HPhb = c.String(),
                        HPhc = c.String(),
                        HPhd = c.String(),
                        WPha = c.String(),
                        WPhb = c.String(),
                        WPhc = c.String(),
                        WPhd = c.String(),
                        MPha = c.String(),
                        MPhb = c.String(),
                        MPhc = c.String(),
                        User_rating = c.String(),
                        Payment_account_id = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.LoginHistoryEntities");
            DropTable("dbo.GenericSettings");
            DropTable("dbo.EmailTemplates");
        }
    }
}
