﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using RentAndTake.Models;


namespace RentAndTake.Context
{
    public class RnTContext : IdentityDbContext<UserEntity>
    {
        public RnTContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static RnTContext Create()
        {
            return new RnTContext();
        }
        //public DbSet<UserEntity> UserEntity { get; set; }
        public DbSet<EmailTemplate> EmailTemplate { get; set; }
        public DbSet<GenericSetting> GenericSetting { get; set; }
        public DbSet<LoginHistoryEntity> LoginHistory { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Product_Photos> product_Photos { get; set; }
        public DbSet<Category> categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Cart> carts { get; set; }
        public DbSet<FinalizedOrders> FinalizedOrders { get; set; }
        public DbSet<UserAddress> userAddresses { get; set; }




    }
}