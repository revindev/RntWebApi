﻿using RentAndTake.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using RentAndTake.Context;


using RentAndTake.Models;
using Microsoft.AspNet.Identity;

using System.Web.Hosting;
using System.IdentityModel.Tokens.Jwt;
using System.Web.Mvc;
using Newtonsoft.Json;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity.Owin;

namespace RentAndTake.Services                                          
{
    public class DefaultUserService : IUserService
    {                        
        #region <variables>                       

        private readonly UserManager<UserEntity> _userManager;                
       // private readonly SignInManager<UserEntity> _signInManager;
       // private readonly IConfiguration _configuration;
        private readonly RnTContext _context;
        private readonly HostingEnvironment _env;                   

        private readonly ICommonService _commonService;
                           
       
      
        private static Random random = new Random();

        #endregion

        #region <Constructor>

        public DefaultUserService(UserManager<UserEntity> userManager, 
             RnTContext RnTContext, ICommonService commonService
              )
        {
            _userManager = userManager;
           // _signInManager = signInManager;
           // _configuration = configuration;
            _context = RnTContext;                 
            _commonService = commonService;                
          
           // _env = env;
           
        }

        #endregion

        #region <User>

        #region methods 

        #region SignUP code
        public async Task<(string result, bool Succeeded)> CreateUserAsync(User form)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                  
                   
                   // int Count = _userManager.Users.Count() + 1;
                   // long ticketid = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssf"));

                    #region UserCreation
                    EmailInformation emailInformationT = new EmailInformation
                    {
                        Browser = form.Browser,
                        IpAddress = form.IpAddress,
                        Location = form.Location,
                        MacAddress = form.MacAddress,

                        EmailAddress = form.EmailID,
                        // ActivationLink = "",
                        UserName = form.EmailID,

                        FullName = form.FirstName+" "+form.LastName,
                      //  ActivationLink = activationlink
                    };
                    var (succededT, errT) = _commonService.SendEmail("WEL", emailInformationT);
                    if (!succededT)
                    {
                        //var response = new { value = "Some server error has occurred. Please try again!!" };
                        return (JsonConvert.SerializeObject(errT), false);

                        // return (JsonConvert.SerializeObject("Your email id is not accessible. Please try again!!"), false);
                    }


                    var entity = new UserEntity
                    {

                        Email = form.EmailID,

                        FirstName = form.FirstName,
                        LastName = form.LastName,
                        UserName = form.EmailID,


                        Status = false,  
                        Date_entered = DateTimeOffset.UtcNow,
                        Birthday = form.Birthday,
                        Birthmonth = form.Birthmonth,
                        Birthyear = form.Birthyear,
                       
                        
                       
                        EmailConfirmed = false
                    };

                    Claim[] claims = new Claim[]
                   {
                    new Claim(ClaimTypes.NameIdentifier, entity.UserName),
                    new Claim(JwtRegisteredClaimNames.Sub, entity.UserName)
                   };

                    var result = _userManager.CreateAsync(entity, form.Password).GetAwaiter().GetResult();

                    if (!result.Succeeded)
                    {
                        var firstError = result.Errors.FirstOrDefault().ToString();
                        // return ( firstError , result.Succeeded);
                        var response = new { value = "Email Id Already Exists. Please Login !!  " };
                        return (JsonConvert.SerializeObject(response), false);
                        //return (JsonConvert.SerializeObject(firstError), result.Succeeded);
                    }
                    else
                    {
                        //result = _userManager.AddToRole(UserLoginInfo, "User");
                        // _userManager.AddToRoleAsync(form.EmailID, "User").Wait();
                         //_userManager.AddClaimsAsync("", claims).Wait();
                        //_userManager.UpdateAsync(entity).Wait();
                        _context.SaveChanges();
                    }
                    
                  
                    //_context.SaveChanges();
                    dbContextTransaction.Commit();

                   
                 
                    {
                        string act = Encrypt((form.EmailID).Trim());
                        //string act = Encrypt((form.FirstName).Trim());
                        string activationlink = string.Empty;

                        string ActLink = System.Configuration.ConfigurationManager.AppSettings["DevActivationLink"];
                        string StagingStatus = System.Configuration.ConfigurationManager.AppSettings["Staging"];
                        string LiveServerAct = System.Configuration.ConfigurationManager.AppSettings["ActivationLink"];
                        if (StagingStatus == "true")
                        {
                            activationlink = ActLink;

                        }
                        else
                        {
                            activationlink = LiveServerAct;
                        }

                       
                        activationlink += act;

                        EmailInformation emailInformation = new EmailInformation
                        {
                            Browser = form.Browser,
                            IpAddress = form.IpAddress,
                            Location = form.Location,
                            MacAddress = form.MacAddress,

                            EmailAddress = form.EmailID,
                            // ActivationLink = "",
                            UserName = form.EmailID,

                            // UserName = result.UserName,
                            ActivationLink = activationlink
                        };

                        var (succeded, err) = _commonService.SendEmail("REG", emailInformation);
                        if (succeded)
                        {
                            var response = new { value = "An Activation email sent successfully on your registered email id. Please check to activate you account." };
                            return (JsonConvert.SerializeObject(response), true);

                        }
                        else
                        {
                            var response = new { value = "Some server error has occurred. Please try again!!" };
                            return (JsonConvert.SerializeObject(err), false);
                            // return ("Some server error has occurred. Please try again!!", false);
                        }
                    }
                   

                    //return (new JsonResult("User has been created successfully."), result.Succeeded);
                    #endregion
                }


                catch (Exception Ex)
                {
                    dbContextTransaction.Rollback();

                    var response = new { value = "Some server error has occurred. Please try again!!" };
                    return (JsonConvert.SerializeObject(response), false);
                }
            }
        }

        #region ActivatingLogin
        public async Task<(string result, bool Succeeded)> ActivateUserLogin(String Code)
        {
            try
            {
               

                string decrypted = Decrypt((Code).Trim());

                var user = _userManager.Users.Where(u => u.Email == decrypted).First();
                if (!user.Status)
                {
                    user.Status = true;
                    user.EmailConfirmed = true;
                    _userManager.UpdateAsync(user).GetAwaiter().GetResult();
                    _context.SaveChangesAsync().GetAwaiter().GetResult();
                    EmailInformation emailInformation = new EmailInformation
                    {
                        UserName = user.UserName,
                        EmailAddress = user.Email
                    };

                    // var response = new {value ="Account successfully activated. Please try to login." };
                    // return (JsonConvert.SerializeObject(response),  true);

                    var response = new { value = "Account successfully activated. Please try to login." };
                    return (JsonConvert.SerializeObject(response), true);


                    //return ("Account successfully activated. Please try to login.", true);
                }
                else
                {
                    var response = new {value = "Account already activated. Please try to login." };
                    return (JsonConvert.SerializeObject(response), false);
                   // return ("Account already activated. Please try to login.", true);
                }
            }
            catch (Exception ex)
            {
                //_logService.writeLog(_env.WebRootPath, ex.ToString());
                var response = new { value = "Invalid Link Please Try Again!!." };
                return (JsonConvert.SerializeObject(response), false);
            }
        }
        #endregion

        #endregion


        #region SignIN code
        

        public async Task<(string result, bool Succeeded)> SignIn(Credentials Credentials)
        {
            try
            {
                var role = string.Empty;
                var useremail = _userManager.FindByEmailAsync(Credentials.UserName).GetAwaiter().GetResult();
                if (useremail != null)
                {
                    if (useremail.UserName.Contains("Admin"))
                    {
                        return ("Authentication failed!! Please check credentials.", false);
                    }
                    if (useremail.UserName.Length > 0)
                    {



                        var result = _userManager.FindAsync(Credentials.UserName, Credentials.Password).GetAwaiter().GetResult();

                        if (result != null)
                        {
                            var user = _userManager.FindByNameAsync(useremail.UserName).GetAwaiter().GetResult();
                            if (user.Status)
                            {

                                var Token = GenerateToken(user);
                                var (success, error) = _commonService.InsertLoginHistory(Credentials, user.Email);
                                if (user.Status)
                                {
                                    // verification link generation and submit 
                                }
                                return (Token, true);
                            }
                            else
                            {
                                var response = new { value = "Please activate your account" };
                                return (JsonConvert.SerializeObject(response), false);
                                //return (("Please activate your account"), false);
                            }
                        }
                        //return (new "Authentication failed!! Please check credentials.", result.Succeeded);
                    }
                    var response1 = new { value = "Authentication failed!! Please check credentials." };
                    return (JsonConvert.SerializeObject(response1), false);

                    //return ( "Authentication failed!! Please check credentials.", false);
                }
                else
                {
                    var response2 = new { value = "Authentication failed!! Please check credentials." };
                    return (JsonConvert.SerializeObject(response2), false);
                }
                //return ("Authentication failed!! Please check credentials.", false);
            }
            catch (Exception Ex)
            {
                //_logService.writeLog(_env.WebRootPath, Ex.ToString());
                //return ("Some server error has occurred. Please try again!!", false);
                var response2 = new { value = "Some server error has occurred. Please try again!!" };
                return (JsonConvert.SerializeObject(response2), false);
            }

        }


        #region Token Generating code
        private string GenerateToken(UserEntity user)
        {



            var now = DateTime.UtcNow;


            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(now).ToUniversalTime().ToString(), ClaimValueTypes.Integer64)

               
            };
          


            string secretKey = System.Configuration.ConfigurationManager.AppSettings["secretKey"];
            string issuer = System.Configuration.ConfigurationManager.AppSettings["Issuer"];
            string audience = System.Configuration.ConfigurationManager.AppSettings["Audience"];
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));



            // Create the JWT and write it to a string
            var jwt = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(TimeSpan.FromMinutes(120)),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

       

            var response = new { access_token = encodedJwt, expires_in = (int)TimeSpan.FromMinutes(200).TotalSeconds, user_name= user.UserName , first_name =user.FirstName, last_name =user.LastName,User_id=user.UserName };
            



           
            var r = new { value = response };

            return  JsonConvert.SerializeObject(r);

        }
        #endregion


        #endregion

        #region address
        public async Task<(string result, bool Succeeded)> AddAddress(AddressForm addressForm)
        {

            // var result = _context.ICODetails.SingleOrDefault(s => s.ICOCode == iCODetails.ICOCode);
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    #region 
                    //string status;
                    //var result = _context.userAddresses.SingleOrDefault(s => s. == cartForm.PostID);
                   // if (result == null)
                    {
                        _context.userAddresses.Add(new UserAddress
                        {
                            FirstName = addressForm.FirstName,
                            LastName = addressForm.LastName,
                            Address1 = addressForm.Address1,
                            Address2 = addressForm.Address2,
                            FullName = addressForm.FirstName + " " + addressForm.LastName,
                            City = addressForm.City,
                            State = addressForm.State,
                            Zip = addressForm.Zip,
                            CreatedDate = DateTime.UtcNow.Date,
                            OwnerId = addressForm.OwnerId,
                            DefaultAddress = addressForm.DefaultAddress,
                            Modifydate = DateTime.UtcNow.Date,
                            status = true
                            //_context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()

                        });
                        //  SaveImage(product_Form.Photos, id + "COVER", fileUrl);

                        _context.SaveChanges();
                        dbContextTransaction.Commit();




                        var Result = new { value = "Add to cart Successfully" };
                        return (JsonConvert.SerializeObject(Result), true);
                    }
                   // else
                   // {
                        //var Result = new { value = "This item is already in your cart" };
                   //     return (JsonConvert.SerializeObject("This item is already in your cart"), false);
                  //  }
                    #endregion
                }
                catch (Exception Ex)
                {
                    string s = Ex.InnerException.Message;
                    string s1 = Ex.InnerException.ToString();


                    dbContextTransaction.Rollback();

                    //var response2 = new { value = "Some server error has occurred. Please try again!!" };
                    return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
                }
            }

        }

        public async Task<(string result, bool Succeeded)> GetAddressByOwnerId(String Bid)
        {
            try
            {

                if (!String.IsNullOrEmpty(Bid))
                {

                    //(DateTime.UtcNow.Date - StartDate).TotalDays
                    //var record = dc.sometabel.Where( u => u.username.Equals("someusername")).SingleOrDefault()
                    var result = (from p in _context.userAddresses
                                  where p.OwnerId == Bid && p.status==true

                                  select new
                                  {
                                      FirstName = p.FirstName,
                                      LastName = p.LastName,
                                      FullName = p.FullName,
                                      Address1 = p.Address1,
                                      Address2 = p.Address2,
                                      DefaultAddress = p.DefaultAddress,
                                      City = p.City,
                                      State = p.State,
                                      Zip = p.Zip,
                                      id = p.id,


                                     
                                  });

                   
                    if (result.Count() > 0)
                    {
                        return (JsonConvert.SerializeObject(result), true);
                    }
                    else
                    {

                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }
                }
                else
                {
                    return (JsonConvert.SerializeObject("No record found!!"), false);
                }

            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return (JsonConvert.SerializeObject("oops Something went wrong!!"), false);
            }

        }

        public async Task<(string result, bool Succeeded)> GetAddressByAddressId(String Bid, string addressId)
        {
            try
            {

                if (!String.IsNullOrEmpty(Bid))
                {

                    //(DateTime.UtcNow.Date - StartDate).TotalDays
                    //var record = dc.sometabel.Where( u => u.username.Equals("someusername")).SingleOrDefault()
                    var result = (from p in _context.userAddresses
                                  where p.OwnerId == Bid && p.id==Convert.ToInt32(addressId) && p.status==true

                                  select new
                                  {
                                      FirstName = p.FirstName,
                                      LastName = p.LastName,
                                      FullName = p.FullName,
                                      Address1 = p.Address1,
                                      Address2 = p.Address2,
                                      DefaultAddress = p.DefaultAddress,
                                      City = p.City,
                                      State = p.State,
                                      Zip = p.Zip,
                                      id = p.id,



                                  });


                    if (result.Count() > 0)
                    {
                        return (JsonConvert.SerializeObject(result), true);
                    }
                    else
                    {

                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }
                }
                else
                {
                    return (JsonConvert.SerializeObject("No record found!!"), false);
                }

            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return (JsonConvert.SerializeObject("oops Something went wrong!!"), false);
            }
        }

        public async Task<(string result, bool Succeeded)> SetDefaultAddress(String Bid, string addressId)
        {
            try
            {

                if (!String.IsNullOrEmpty(Bid))
                {

                    //(DateTime.UtcNow.Date - StartDate).TotalDays
                    //var record = dc.sometabel.Where( u => u.username.Equals("someusername")).SingleOrDefault()

                    int aID = Convert.ToInt32(addressId);
                    var result = _context.userAddresses.SingleOrDefault(s => s.id == aID && s.OwnerId == Bid && s.OwnerId == Bid);



                  //  var result = _context.userAddresses.SingleOrDefault(s => s.id == Convert.ToInt32(addressId) && s.OwnerId==Bid );
                    var result2 = _context.userAddresses.Where(s => s.OwnerId == Bid).Select(s=>s.id).ToList();

                    if (result != null)
                    {
                        var useradd = _context.userAddresses.Where(f => result2.Contains(f.id)).ToList();
                        useradd.ForEach(a => a.DefaultAddress = "0");
                        _context.SaveChanges();
                        

                        result.DefaultAddress = "1";
                        _context.SaveChanges();
                        var Result = new { value = "Address Is Set To Default Successfully!!" };
                        return (JsonConvert.SerializeObject(Result), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }
                    
                }
                else
                {
                    return (JsonConvert.SerializeObject("No record found!!"), false);
                }

            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return (JsonConvert.SerializeObject("oops Something went wrong!!"), false);
            }
        }


        public async Task<(string result, bool Succeeded)> DeleteAddress(String Bid, string addressId)
        {
            try
            {

                if (!String.IsNullOrEmpty(Bid) && !string.IsNullOrEmpty(addressId))
                {

                    int aID = Convert.ToInt32(addressId);
                    var result = _context.userAddresses.FirstOrDefault(s => s.id == aID && s.OwnerId == Bid && s.DefaultAddress!="1");
                  
                    if (result != null)
                    {
                        result.status = false;
                        result.Modifydate = DateTime.UtcNow.Date;
                     
                        _context.SaveChanges();
                        var Result = new { value = "Address Deleted Successfully!!" };
                        return (JsonConvert.SerializeObject(Result), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }

                }
                else
                {
                    return (JsonConvert.SerializeObject("No record found!!"), false);
                }

            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return (JsonConvert.SerializeObject("oops Something went wrong!!"), false);
            }
        }

        public async Task<(string result, bool Succeeded)> UpdateAddress(string AddressId, AddressForm addressForm)
        {


            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    #region 
                    if (!String.IsNullOrEmpty(addressForm.OwnerId) && !string.IsNullOrEmpty(AddressId))
                    {
                        int aid = Convert.ToInt32(AddressId);
                        var result = _context.userAddresses.SingleOrDefault(s => s.id == aid && s.OwnerId == addressForm.OwnerId);
                        if (result != null)
                        {
                            // result.status = false;
                            result.Modifydate = DateTime.UtcNow.Date;
                            result.FirstName = addressForm.FirstName;
                            result.LastName = addressForm.LastName;
                            result.Address1 = addressForm.Address1;
                            result.Address2 = addressForm.Address2;
                            result.City = addressForm.City;
                            result.State = addressForm.State;
                            result.status = true;

                            _context.SaveChanges();
                            dbContextTransaction.Commit();

                            var Result = new { value = "Address Updated Successfully!!" };
                           // return (JsonConvert.SerializeObject(Result), true);
                            return (JsonConvert.SerializeObject(Result), true);
                        }
                        else
                        {
                            return (JsonConvert.SerializeObject("No record found!!"), false);
                        }

                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }
                    
                    #endregion
                }
                catch (Exception Ex)
                {
                    string s = Ex.InnerException.Message;
                    string s1 = Ex.InnerException.ToString();


                    dbContextTransaction.Rollback();

                    //var response2 = new { value = "Some server error has occurred. Please try again!!" };
                    return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
                }
            }

        }

        #endregion

        // public (JsonResult result, bool Succeeded) ForgetPassLink(string email)



       



        public async Task<(string result, bool Succeeded)> ForgetPassLink(String email)
        {

            try
            {
                if (!_context.Users.Where(x => x.Email == email).Any())
                {
                  //  return (new JsonResult("Email id not correct. Please try again."), false);
                    return (JsonConvert.SerializeObject("Email id not correct. Please try again."), true);
                }
                var result = _userManager.FindByEmailAsync(email).GetAwaiter().GetResult();
                if (!result.Status)
                {
                    return (JsonConvert.SerializeObject("Please activate your account."), false);
                }
                if (result != null)
                {
                   
                    Guid guid = Guid.NewGuid();

                    //var provider = new DpapiDataProtectionProvider("YourAppName");
                    //_userManager.UserTokenProvider = new DataProtectorTokenProvider<User, string>(provider.Create("UserToken"))
                    //    as IUserTokenProvider<User, string>;
                    //var provider = new DpapiDataProtectionProvider("SampleAppName");

                    //var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>());

                    //userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(
                    //    provider.Create("SampleTokenName"));

                    //_userManager.UserTokenProvider =new DataProtectorTokenProvider<User,string>()

                    //string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id.ToString());

                    var provider = new DpapiDataProtectionProvider("RentAndTake");
                    _userManager.UserTokenProvider = new DataProtectorTokenProvider<UserEntity, string>(provider.Create("UserToken")) as IUserTokenProvider<UserEntity, string>;

                    var token = _userManager.GeneratePasswordResetTokenAsync(result.Id);
                    if (token.Result != null || token.Result != "")
                    {
                         string forgetpasswordlink = (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Staging"]) ? System.Configuration.ConfigurationManager.AppSettings["DevActivationLink"] : System.Configuration.ConfigurationManager.AppSettings["ActivationLink"]);

                        forgetpasswordlink += result.Email.Replace("@", ":") + "&token=" + System.Web.HttpUtility.UrlEncode(token.Result);
                        EmailInformation emailInformation = new EmailInformation
                        {
                            AccountNo = result.UserName,
                            EmailAddress = email,
                            UserName = result.UserName,
                            ResetPassLink = forgetpasswordlink
                        };
                        var (Succeeded, Error) = _commonService.SendEmail("RESPAS", emailInformation);
                        if (Succeeded)
                        {
                            return (JsonConvert.SerializeObject("Password reset link has been sent on your registered email id."), true);
                        }
                        else
                            return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
                    }
                    return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
                }
                else
                    return (JsonConvert.SerializeObject("Please try with valid email id."), false);
            }
            catch (Exception Ex)
            {
                //_logService.writeLog(_env.WebRootPath, Ex.ToString());
                return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
            }

        }

        public async Task<(string result, bool Succeeded)> SetForgetPass(ResetPassword resetPassword)
        {
            try
            {
                var user = _userManager.FindByEmailAsync(resetPassword.Useremail).GetAwaiter().GetResult();
                if (user != null)
                {
                    var changepass = _userManager.ResetPasswordAsync(user.Id, resetPassword.UserToken, resetPassword.UserPassword).GetAwaiter().GetResult();
                    if (changepass.Succeeded)
                    {
                        EmailInformation emailInformation = new EmailInformation
                        {
                            EmailAddress = resetPassword.Useremail,
                            UserName = user.UserName
                        };
                        var (Succeeded, Error) = _commonService.SendEmail("PASCNG", emailInformation);
                        //return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
                        return (JsonConvert.SerializeObject("Password has been changed successfully."),true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject(changepass.Errors.FirstOrDefault().Distinct()), false);
                    }
                    

                }
                else
                {
                    return (JsonConvert.SerializeObject("Invalid link. Please try again!!"), false);
                }
                // return (JsonConvert.SerializeObject(("Invalid link. Please try again!!"), false);
            }
            catch (Exception Ex)
            {
                // _logService.writeLog(_env.WebRootPath, Ex.ToString());
                return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
            }
        }



        public (JsonResult result, bool Succeeded) ResendActivationLink(ResendActivationLink resendActivationLink)
        {
            throw new NotImplementedException();
        }
        public (JsonResult result, bool Succeeded) GetAllUsers()
        {
            throw new NotImplementedException();
        }
        public (JsonResult result, bool Succeeded) GetUserProfile(ClaimsPrincipal user)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region OtherFunction

        public string Decrypt(string encryptText)
        {
            try
            {
                string encryptionkey = System.Configuration.ConfigurationManager.AppSettings["EncryptionKey"];
                //string encryptionkey = _configuration.GetSection("Encryption:Key").Value;



                byte[] keybytes = Encoding.ASCII.GetBytes(encryptionkey.Length.ToString());
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                byte[] encryptedData = Convert.FromBase64String(encryptText.Replace(" ", "+"));
                PasswordDeriveBytes pwdbytes = new PasswordDeriveBytes(encryptionkey, keybytes);
                using (ICryptoTransform decryptrans = rijndaelCipher.CreateDecryptor(pwdbytes.GetBytes(32), pwdbytes.GetBytes(16)))
                {
                    using (MemoryStream mstrm = new MemoryStream(encryptedData))
                    {
                        using (CryptoStream cryptstm = new CryptoStream(mstrm, decryptrans, CryptoStreamMode.Read))
                        {
                            byte[] plainText = new byte[encryptedData.Length];
                            int decryptedCount = cryptstm.Read(plainText, 0, plainText.Length);
                            return Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                //_logService.writeLog(_env.WebRootPath, Ex.ToString());
                return Ex.Message;
            }
        }

        public string Encrypt(string inputText)
        {
            try
            {
                string encryptionkey = System.Configuration.ConfigurationManager.AppSettings["EncryptionKey"];
               // string encryptionkey = _configuration.GetSection("Encryption:Key").Value;
                byte[] keybytes = Encoding.ASCII.GetBytes(encryptionkey.Length.ToString());
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                byte[] plainText = Encoding.Unicode.GetBytes(inputText);
                PasswordDeriveBytes pwdbytes = new PasswordDeriveBytes(encryptionkey, keybytes);
                using (ICryptoTransform encryptrans = rijndaelCipher.CreateEncryptor(pwdbytes.GetBytes(32), pwdbytes.GetBytes(16)))
                {
                    using (MemoryStream mstrm = new MemoryStream())
                    {
                        using (CryptoStream cryptstm = new CryptoStream(mstrm, encryptrans, CryptoStreamMode.Write))
                        {
                            cryptstm.Write(plainText, 0, plainText.Length);
                            cryptstm.Close();
                            return Convert.ToBase64String(mstrm.ToArray());
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                //_logService.writeLog(_env.WebRootPath, Ex.ToString());
                return Ex.Message;
            }
        }


        //private JsonResult Errors(IdentityResult result)
        //{
        //    var items = result.Errors
        //        .Select(x => x.Description)
        //        .ToArray();
        //    return new JsonResult(items) { StatusCode = 400 };
        //}

        //private JsonResult Error(string message)
        //{
        //    return new JsonResult(message) { StatusCode = 400 };
        //}

        public string GetUserID(ClaimsPrincipal user)
        {
            throw new NotImplementedException(); ;
        }



        #endregion

        #endregion


    }
}
