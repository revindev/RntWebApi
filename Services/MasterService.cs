﻿using RentAndTake.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using RentAndTake.Context;


using RentAndTake.Models;
using Microsoft.AspNet.Identity;

using System.Web.Hosting;
using System.IdentityModel.Tokens.Jwt;
using System.Web.Mvc;
using Newtonsoft.Json;
using Microsoft.IdentityModel.Tokens;

namespace RentAndTake.Services
{
    public class MasterService:IMasterService
    {

        #region global variables and constructors 
        private readonly RnTContext _context;

       
        private static Random random = new Random();

        public MasterService(RnTContext RnTContext)
        {
            _context = RnTContext;
        }
        #endregion




        #region get functions

        public async Task<(string result, bool Succeeded)> GetAllCategory(ClaimsPrincipal user)
        {
            try
            {
                //var flag = _configuration.GetSection("Erc20TokenUrl:CountryFlag").Value.ToString();
                var result = (from c in _context.categories
                              orderby c.CategoryName ascending
                              //where !c.IsRestricted
                              select new
                              {
                                  value=c.Id,
                                  label = c.CategoryName
                                  
                              });

                if (result.Count() > 0)
                {
                    return (JsonConvert.SerializeObject(result), true);
                }
                else
                {
                    return (JsonConvert.SerializeObject(result), true);
                }
            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return ("", true);
            }

            //var Result = new { value = "Add is Posted Successfully" };
           
        }
        public async Task<(string result, bool Succeeded)> GetSubCategory(string cid, ClaimsPrincipal user)
        {
            try
            {
               

                var result = (from c in _context.SubCategories
                              where c.Cid == cid
                              select new
                              {
                                  value = c.Id
                                  ,
                                  label=c.SubCategoryName
                              });

                if (result.Count() > 0)
                {
                    return (JsonConvert.SerializeObject(result), true);
                }
                else
                {
                    return (JsonConvert.SerializeObject(result), true);
                }
            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return ("", true);
            }

        }
        #endregion


        #region post functions

        public async Task<(string result, bool Succeeded)> AddCategory(CategoryEntryForm categoryEntryForm, ClaimsPrincipal user)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {


                    // int Count = _userManager.Users.Count() + 1;
                    long ticketid = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssf"));
                    var CId = Guid.NewGuid();
                    var CatID = "CAT-" + DateTime.Now.ToString("yyyyMMddHHmmssfff");


                    _context.categories.Add(new Category
                    {
                        CId = CId,
                        
                        Id= CatID.ToString(),
                        CategoryName = categoryEntryForm.CategoryName,
                        Status=true,
                        Date_entered = DateTime.UtcNow.Date,
                        Date_modified = DateTime.UtcNow.Date

                    });
                    _context.SaveChanges();
                    dbContextTransaction.Commit();

                    var Result = new { value = "Saved Successfully" };
                    return (JsonConvert.SerializeObject(Result), true);
                }

                catch (Exception Ex)
                {
                    string s = Ex.InnerException.Message;
                    string s1 = Ex.InnerException.ToString();


                    dbContextTransaction.Rollback();

                    var response2 = new { value = "Some server error has occurred. Please try again!!" };
                    return (JsonConvert.SerializeObject(response2), false);
                }

            }
        }
      


        public async Task<(string result, bool Succeeded)> AddSubCategory(SubCategoryEntryForm subCategoryEntryForm, ClaimsPrincipal user)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {


                    // int Count = _userManager.Users.Count() + 1;
                    long ticketid = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssf"));
                    var SCId = Guid.NewGuid();
                    var SubCatID = "SUBCAT-" + DateTime.Now.ToString("yyyyMMddHHmmssfff");


                    _context.SubCategories.Add(new SubCategory
                    {
                        SCId = SCId,
                        Id= SubCatID,
                        Cid = subCategoryEntryForm.Cid,
                        SubCategoryName = subCategoryEntryForm.SubCategoryName,
                        Status = true,
                        Date_entered = DateTime.UtcNow.Date,
                        Date_modified = DateTime.UtcNow.Date

                    });
                    _context.SaveChanges();
                    dbContextTransaction.Commit();

                    var Result = new { value = "Saved Successfully" };
                    return (JsonConvert.SerializeObject(Result), true);
                }

                catch (Exception Ex)
                {
                    string s = Ex.InnerException.Message;
                    string s1 = Ex.InnerException.ToString();


                    dbContextTransaction.Rollback();

                    var response2 = new { value = "Some server error has occurred. Please try again!!" };
                    return (JsonConvert.SerializeObject(response2), false);
                }

            }
        }
        #endregion
        public async Task<(string result, bool Succeeded)> GetNavBar()
        {
            try
            {
                var result = (from c in _context.categories
                              orderby c.CategoryName ascending
                              select new
                              {
                                  CategoryId = c.Id,
                                  CategoryName = c.CategoryName,
                                  SubCategory=_context.SubCategories.Where(x=>x.Cid==c.Id).Select(x=> new { SubCategoryId = x.Id,SubCategoryName = x.SubCategoryName}).ToList()
                              });

                if (result.Count() > 0)
                {
                    return (JsonConvert.SerializeObject(result), true);
                }
                else
                {
                    return (JsonConvert.SerializeObject("No record found!!!"), true);
                }
            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return ("", true);
            }
        }

    }
}