﻿
//using Microsoft.Extensions.Configuration;
using RentAndTake.Context;
using System;
using System.IO;
using System.Web;

namespace RentAndTake.Services
{
    public class LogService : ILogService
    {
        #region <Private Variables>

        private readonly RnTContext _RnTContext;
    
        private string sLogFormat;
        private string sErrorTime;
        #endregion

        #region <Constructor>

        public LogService(RnTContext RnTContext)
        {
            _RnTContext = RnTContext;
            //_env = env;
            //_configuration = configuration;

            //sLogFormat used to create log files format :
            // dd/mm/yyyy hh:mm:ss AM/PM ==> Log Message
            sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

            //this variable used to create log filename format "
            //for example filename : ErrorLogYYYYMMDD
            string sYear = DateTime.Now.Year.ToString();
            string sMonth = DateTime.Now.Month.ToString();
            string sDay = DateTime.Now.Day.ToString();
            string sHur = DateTime.Now.Hour.ToString();
            string sMin = DateTime.Now.Minute.ToString();
            sErrorTime = sYear + sMonth + sDay;// + sHur + sMin;
        }


        #endregion

        #region <Methods>

        public void ErrorLog(string sPathName, string sErrMsg)
        {
            #region MyRegion
            //StreamWriter sw = new StreamWriter(sPathName + sErrorTime, true);
            //sw.WriteLine(sLogFormat + sErrMsg);
            //sw.Flush();
            //sw.Close();
            //if (!File.Exists(Path))
            //{
            //    FileStream FS = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.Write);
            //    StreamWriter writer = new StreamWriter(FS);
            //    writer.Write(sErrMsg);
            //    writer.Close();
            //}
            #endregion
            if (!Directory.Exists(sPathName))
            {
                Directory.CreateDirectory(sPathName);
            }
            String Path = sPathName + sErrorTime + ".txt";
            if (File.Exists(Path))
            {
                using (StreamWriter writer = new StreamWriter(Path, true))
                {
                    writer.WriteLine(sErrMsg + DateTime.Now);
                }
            }
            else
            {
                StreamWriter writer = File.CreateText(Path);
                writer.WriteLine(sErrMsg + DateTime.Now);
                writer.Close();
            }


        }
        public void writeLog(string path, String strlog, string AccountNo = "")
        {
            //_context.Log.Add(new LogEntity
            //{
            //    ID = Guid.NewGuid(),
            //    LogMessage = strlog,
            //    CreatedAt = DateTimeOffset.Now,
            //    AccountNO = AccountNo

            //});
            //_context.SaveChanges();
            ErrorLog(path + "Logs/ErrorLogTrace", strlog);

        }


        public bool SaveImage(string ImgStr, string ImgName,string path1)
        {
            String path = HttpContext.Current.Server.MapPath("~/ImageStorage"); //Path

            //Check if directory exist
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
            }

            string imageName = ImgName + ".jpg";

            //set the image path
            string imgPath = Path.Combine(path, imageName);

            byte[] imageBytes = Convert.FromBase64String(ImgStr);

            File.WriteAllBytes(imgPath, imageBytes);

            return true;
        }

        #endregion
    }
}
