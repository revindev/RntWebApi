﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentAndTake.Services
{

    public class OrderForm
    {
        public List<CartItems> Items { get; set; }
    }
    public class CartItems
    {
        public int CartId { get; set; }     // this will be a child order  ItemOrderId
        public string PostId { get; set; }
        public string MessageToOwner { get; set; }
        public string BuyerId { get; set; }
        public string  ShippingAddress { get; set; }
    }
}