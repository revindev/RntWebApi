﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RentAndTake.Models;
using System.Threading.Tasks;
using RentAndTake.Context;
using Newtonsoft.Json;

namespace RentAndTake.Services
{
    public class CartService : IcartService
    {
        private readonly RnTContext _context;
        private static Random random = new Random();
        private Product _Product;       

        public CartService(RnTContext RnTContext, Product Product)
        {
            _context = RnTContext;
            _Product = Product;
        }
        // public Task<(string result, bool Succeeded)> AddToCart(CartForm cartForm)
        public async Task<(string result, bool Succeeded)> AddToCart(CartForm cartForm)
        {

            // var result = _context.ICODetails.SingleOrDefault(s => s.ICOCode == iCODetails.ICOCode);
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    #region 
                    //string status;
                    var result = _context.carts.SingleOrDefault(s => s.PostID == cartForm.PostID && s.BuyerID==cartForm.BuyerID);
                    if (result == null)
                    {
                        _context.carts.Add(new Cart
                        {
                            AddToCartDate = DateTime.UtcNow.Date,
                            ModifyCartDate = DateTime.UtcNow.Date,

                            SubAdType = cartForm.SubAdType,
                            AdType = cartForm.AdType,

                            BuyerID = cartForm.BuyerID,
                            PostID = cartForm.PostID,
                            //OwnerId=_context.Products.Where(y=>y.Id==cartForm.PostID).Select(y=>y.Owner_Id).SingleOrDefault()
                            OwnerId = cartForm.OwnerId,

                            //_context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()

                        });
                        //  SaveImage(product_Form.Photos, id + "COVER", fileUrl);

                        _context.SaveChanges();
                        dbContextTransaction.Commit();

                        var Result = new { value = "Add to cart Successfully" };
                        return (JsonConvert.SerializeObject(Result), true);
                    }
                    else
                    {
                        //var Result = new { value = "This item is already in your cart" };
                        return (JsonConvert.SerializeObject("This item is already in your cart"), false);
                    }
                    #endregion
                }
                catch (Exception Ex)
                {
                    string s = Ex.InnerException.Message;
                    string s1 = Ex.InnerException.ToString();


                    dbContextTransaction.Rollback();

                    //var response2 = new { value = "Some server error has occurred. Please try again!!" };
                    return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
                }
            }

        }
        

        public async Task<(string result , bool Succeeded)> AddToCartMultipleItem(CartFinalizeForm cartFinalizeForm)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var a = cartFinalizeForm.Items;

                    foreach (var item in cartFinalizeForm.Items)
                    {
                        //   var (result, succeeded) = _cartService.AddToCartMultipleItem(cartFinalizeForm).GetAwaiter().GetResult();

                        DeletCart(null, item.BuyerID).GetAwaiter().GetResult();
                        break;
                    }

                    //var items = _context.carts.Where(x => x.OwnerId == a[0].OwnerId.ToString());
                    //_context.carts.RemoveRange(items);
                    List<Cart> listCart = new List<Cart>();

                    foreach (var item in a)
                    {
                        listCart.Add(new Cart
                        {
                            AddToCartDate = DateTime.UtcNow.Date,
                            ModifyCartDate = DateTime.UtcNow.Date,

                            SubAdType = item.SubAdType,
                            AdType = item.AdType,

                            BuyerID = item.BuyerID,
                            PostID = item.PostID,
                            //OwnerId=_context.Products.Where(y=>y.Id==cartForm.PostID).Select(y=>y.Owner_Id).SingleOrDefault()
                            OwnerId = item.OwnerId,
                            Price = item.price.ToString(),
                            DeliveryPayment = item.DeliveryPayment,
                            Total = item.Total,
                            DeliveryMethods = item.DeliveryType,
                            HoursOrDays = item.HoursOrDays,
                            ShipingDirection = item.ShipingDirection,
                            OwnerName = _context.Users.Where(y => y.UserName == item.OwnerId).Select(y => y.FirstName+" "+y.LastName).SingleOrDefault().ToString(),
                            BuyerOrRenterName = _context.Users.Where(y => y.UserName == item.BuyerID).Select(y => y.FirstName + " " + y.LastName).SingleOrDefault().ToString(),
                            SubTotal=item.SubTotal,
                            

                            //                     public decimal price { get; set; }
                            //public string DeliveryType { get; set; }
                            //public string DeliveryPayment { get; set; }
                            //public decimal Total { get; set; }
                            //_context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()

                        });
                       

                       
                    }
                    _context.carts.AddRange(listCart);
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    var Result = new { value = "Cart Updated Successfully" };
                    return (JsonConvert.SerializeObject(Result), true);
                }
                catch(Exception ex)
                {
                    string msg = ex.Message;
                    dbContextTransaction.Rollback();
                    return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
                }
            }
        }

        public async Task<(string result, bool Succeeded)> GetCartByBuyerId(String Bid)
        {
            try
            {

                if (!String.IsNullOrEmpty(Bid))
                {
                   
                    //(DateTime.UtcNow.Date - StartDate).TotalDays
                    //var record = dc.sometabel.Where( u => u.username.Equals("someusername")).SingleOrDefault()
                    var result = (from p in _context.carts
                                  where p.BuyerID == Bid 

                                  select new
                                  {

                                      Cartid = p.id,
                                      Buyerid = p.BuyerID,
                                      SubAdtype = p.SubAdType,
                                      price=p.Price,
                                      TotalPrice=p.Total,
                                      HoursOrDays=p.HoursOrDays,
                                      DeliveryPayment=p.DeliveryPayment,
                                      DeliveryMethods= p.DeliveryMethods,
                                      ShippingDirection=p.ShipingDirection,
                                      SubTotal=p.SubTotal,
                                      address=string.IsNullOrEmpty( _context.userAddresses.Where(y=>y.OwnerId==p.BuyerID && y.status==true && y.DefaultAddress=="1").Select(y=>y.FullName +" "+ y.Address1+" "+y.Address2 +" "+y.City+" "+y.State+" "+y.Zip ).Distinct().FirstOrDefault()) ? _context.userAddresses.Where(y => y.OwnerId == p.BuyerID && y.status == true).Select(y => y.FullName + " " + y.Address1 + " " + y.Address2 + " " + y.City + " " + y.State + " " + y.Zip).Distinct().FirstOrDefault() : _context.userAddresses.Where(y => y.OwnerId == p.BuyerID && y.status == true && y.DefaultAddress == "1").Select(y => y.FullName + " " + y.Address1 + " " + y.Address2 + " " + y.City + " " + y.State + " " + y.Zip).Distinct().FirstOrDefault(),
                                      product = _context.Products.Where(y => y.Id == p.PostID.ToString()).Select(y => new
                                      {
                                          Sellerid = y.Owner_Id,
                                          photos = y.Photos + ".png",
                                          Adtype = y.Ad_type,
                                          RentDnegotiable = y.Rent_D_negotiable,
                                          RentHnegotiable = y.Rent_H_negotiable,
                                          Salenegotiable = y.Sale_Negotiable,
                                          Sale_Amount = y.Sale_Amount,
                                          Rent_by_hour = y.rent_by_hour,
                                          Rent_by_day = y.rent_by_day,
                                          Display_Name = y.Display_Name,
                                          LocCity_State_Country = y.Loc_City_State_Country,
                                          Ship = y.Ship,
                                          productId=y.Id,
                                          OwnerId=y.Owner_Id,
                                          
                                          




                                      })
                                  });


                    if (result.Count() > 0)
                    {
                        return (JsonConvert.SerializeObject(result), true);
                    }
                    else
                    {

                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }
                }
                else
                {
                    return (JsonConvert.SerializeObject("No record found!!"), false);
                }

            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return (JsonConvert.SerializeObject("oops Something went wrong!!"), false);
            }

        }

        public async Task<(string result, bool Succeeded)> DeletCart(String PostId, string BuyerID)
        {
            try
            {
                bool checkPostId = false;
                if (!String.IsNullOrEmpty(PostId))
                {
                    if (!String.IsNullOrEmpty(PostId.Trim()))
                    {
                        checkPostId = true;
                    }
                }
                if (checkPostId)
                {
                    var itemToRemove = _context.carts.SingleOrDefault(x => x.PostID == PostId && x.BuyerID == BuyerID); //returns a single item.

                    if (itemToRemove != null)
                    {
                        var Result = new { value = "Removed From Cart" };
                        _context.carts.Remove(itemToRemove);
                        _context.SaveChanges();

                      


                        return (JsonConvert.SerializeObject(Result), true);

                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("Already Removed"), false);
                    }
                }
                else
                {
                    #region For Delete cart
                    var itemToRemove = _context.carts.Where(x => x.BuyerID == BuyerID); //returns a single item.

                    if (itemToRemove != null)
                    {
                        var Result = new { value = "Rermoved From Cart" };
                        _context.carts.RemoveRange(itemToRemove.ToList());
                        _context.SaveChanges();
                        return (JsonConvert.SerializeObject(Result), true);

                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("Cart is Already Empty"), false);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return (JsonConvert.SerializeObject(msg), false);
            }



        }

        public async Task<(string result, bool Succeeded)> PlaceOrderFinal(OrderForm orderForm)
        {
            
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var buID = orderForm.Items[0].BuyerId;
                    var a = _context.carts.Where(x => x.BuyerID == buID).ToList();
                    // var x1 = GetCartByBuyerId(orderForm.Items[0].BuyerId);

                    //var a = orderForm.Items;


                    //var items = _context.carts.Where(x => x.OwnerId == a[0].OwnerId.ToString());
                    //_context.carts.RemoveRange(items);
                    List<OrderForm> listCart = new List<OrderForm>();
                    string itemOrderId = "RNT-ORDER-";
                    //  string OrderId = "RNT-ORDER-";
                    var OrderId = "RNT-ORDER-" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

                    foreach (var item in a)
                    {
                       // string adtyep = _context.carts.Where(y => y.PostID == item.PostId && y.id == item.CartId).Select(y => y.SubAdType);
                        _context.FinalizedOrders.Add(new FinalizedOrders
                        {
                            Orderdate = DateTime.UtcNow.Date,
                            MasterId= OrderId,
                            ItemOrderId =itemOrderId+item.id.ToString(),
                            PostId = item.PostID,
                            OwnerId = item.OwnerId,
                            OwnerName = item.OwnerName,
                            BuyerId = item.BuyerID,
                            BuyerName= item.BuyerOrRenterName,
                            SubAdType = Convert.ToInt32(item.SubAdType),
                            Price =Convert.ToDecimal(item.Price),
                            HoursOrDays=item.HoursOrDays,
                            DeliveryMethods=item.DeliveryMethods,
                            ShipingDirection=item.ShipingDirection,
                            DeliveryPayment=item.DeliveryPayment,
                            Total=item.Total,
                            SubTotal=item.SubTotal,
                            MessageToOwner= string.IsNullOrEmpty( (orderForm.Items.Where(y=>y.PostId==item.PostID).Select(y=>y.MessageToOwner).SingleOrDefault().ToString()))? "" : orderForm.Items.Where(y => y.PostId == item.PostID).Select(y => y.MessageToOwner).SingleOrDefault().ToString()
                            ,
                            ShippingAddress=string.IsNullOrEmpty( orderForm.Items.Where(y => y.PostId == item.PostID).Select(y => y.ShippingAddress).SingleOrDefault().ToString())?"": orderForm.Items.Where(y => y.PostId == item.PostID).Select(y => y.ShippingAddress).SingleOrDefault().ToString()
                        });                      

                    }
                    // change the status of products 
                    foreach (var item in a)
                    {
                        var result = _context.Products.SingleOrDefault(s => s.Id == item.PostID);
                        if (result != null)
                        {
                            result.Status = false;
                          //  result.PerUserTokenLimit = iCODetails.PerUserTokenLimit;
                            _context.SaveChanges();
                          //  return (true, "");
                        }
                       // return (false, "Cannot update the data.");

                    }


                    // _context.carts.AddRange(listCart);
                    //  _context.SaveChanges();
                    // dbContextTransaction.Commit();

                    // var cartitems = _context.carts.Where(y => y.BuyerID == orderForm.Items[0].BuyerId);
                    foreach (var item in a)
                    {
                         // var (result1, succeeded1) = _cartService.AddToCartMultipleItem(cartFinalizeForm).GetAwaiter().GetResult();

                       DeletCart(null, item.BuyerID).GetAwaiter().GetResult();
                       break;
                    }


                    _context.SaveChanges();
                    dbContextTransaction.Commit();




                    var Result = new { value = "Order Placed Successfully" };
                    return (JsonConvert.SerializeObject(Result), true);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    dbContextTransaction.Rollback();
                     return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false  );
                   // var Result = new { value = "Order Placed Successfully" };
                    //return (JsonConvert.SerializeObject(Result), true);
                }
            }
            }

        }

    }

