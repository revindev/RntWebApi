﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentAndTake.Models;

namespace RentAndTake.Services
{
    public interface IcartService
    {
        Task<(string result, bool Succeeded)> AddToCart(CartForm cartForm );
        Task<(string result, bool Succeeded)> AddToCartMultipleItem(CartFinalizeForm cartFinalizeForm);

        Task<(string result, bool Succeeded)> GetCartByBuyerId(String Bid);

      //  Task<(string result, bool Succeeded)> DeletItemIntheCart(String PostId,string BuyerID);

        Task<(string result, bool Succeeded)> DeletCart(String PostId, string BuyerID);

       Task<(string result, bool Succeeded)> PlaceOrderFinal(OrderForm orderForm);





        // Task<(string result, bool Succeeded)> GetCartByBuyerId1(String Bid);

    }
}
