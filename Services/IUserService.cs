﻿using RentAndTake.Models;

using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace RentAndTake.Services
{
    public interface IUserService
    {
       // used or implimented
       // (string result, bool Succeeded) CreateUserAsync(User form);
        Task<(string result, bool Succeeded)> CreateUserAsync(User form);

        Task<(string result, bool Succeeded)> SignIn(Credentials Credentials);
     //  (JsonResult result, bool Succeeded) VerifyUserEmail(String Code);
       //(string result, bool Succeeded) ActivateUserLogin(String Code);
        Task<(string result, bool Succeeded)> ActivateUserLogin(String Code);

        Task<(string result, bool Succeeded)> AddAddress(AddressForm addressForm);


        Task<(string result, bool Succeeded)> GetAddressByOwnerId(String id);
        Task<(string result, bool Succeeded)> GetAddressByAddressId(String Ownerid,string addressId);
        Task<(string result, bool Succeeded)> SetDefaultAddress(String Ownerid, string addressId);

        Task<(string result, bool Succeeded)> DeleteAddress(String Ownerid, string addressId);
        Task<(string result, bool Succeeded)> UpdateAddress(string AddressId ,AddressForm addressForm);











        // un-used or pending to impliment 
        // (JsonResult result, bool Succeeded) ResendActivationLink(ResendActivationLink resendActivationLink); 
        // (JsonResult result, bool Succeeded) GetUserProfile(ClaimsPrincipal user);
        Task<(string result, bool Succeeded)> ForgetPassLink(String email);
        Task<(string result, bool Succeeded)> SetForgetPass(ResetPassword resetPassword);
        // (JsonResult result, bool Succeeded) GetAllUsers();

    }
}
