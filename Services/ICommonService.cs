﻿using RentAndTake.Models;
using System;

namespace RentAndTake.Services
{
    public interface ICommonService
    {
       

 

      

        (String result, bool Succeeded, string Error) FileToString(string FilePath);
         string Decrypt(string encryptText);
         string Encrypt(string inputText);
        (bool Succeeded, string Error) SendEmail(string TemplateName, EmailInformation emailInformation, bool isAdmin = false);
        (bool Succeeded, string Error) InsertLoginHistory(Credentials credentials, string accountNo);

    }
}
