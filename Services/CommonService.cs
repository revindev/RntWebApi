﻿using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using RentAndTake.Context;
using RentAndTake.Models;
using System.Web.Hosting;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace RentAndTake.Services
{
    public class CommonService : ICommonService
    {
        #region <Private Variables>

        private readonly RnTContext _context;
        //private IHostingEnvironment _env;
        //private IConfiguration _configuration;
       

        #endregion

        #region <Constructor>

        public CommonService(RnTContext context)
        {
            _context = context;
            //_env = env;
            //_configuration = configuration;
           
        }

        #endregion

        #region <Methods>


        public (bool Succeeded, string Error) InsertLoginHistory(Credentials credentials, string accountNo)
        {
            try
            {

                var LoginHistory = _context.LoginHistory.Add(new LoginHistoryEntity
                {
                    ID = Guid.NewGuid(),
                    AccountNo = accountNo,
                    Browser = credentials.Browser,
                    
                    IpAddress = credentials.IpAddress,
                    Location = credentials.Location,
                    MacAddress = credentials.MacAddress,
                    LoginTime = DateTime.Now
                });

               

                if (_context.SaveChanges() > 0)
                {
                    return (true, "");
                }
                return (false, "Problem in history logging");
            }
            catch (Exception Ex)
            {
                //_logService.writeLog(_env.WebRootPath, Ex.ToString());
                return (false, Ex.Message);
            }
        }


        public (bool Succeeded, string Error) SendEmail(string TemplateCode, EmailInformation emailInformation, bool isAdmin = false)
        {

            bool flag = false;
            string error = "";
            try
            {
                if (TemplateCode != "")
                {
                    var result = _context.EmailTemplate.Where(s => s.TemplateCode == TemplateCode).First<EmailTemplate>();
                    if (result.TemplateCode != null)
                    {
                        error = SendEmail(result, emailInformation, isAdmin);
                        if (error == "Success") { flag = true; }
                    }

                }
                return (flag, error);
            }
            catch (Exception Ex)
            {

                return (flag, Ex.Message);
            }
        }


        private string SendEmail(EmailTemplate result, EmailInformation emailInformation, bool isAdmin)
        {
            string error = "";
            string body = _context.GenericSetting.Where(s => s.SettingName == "EMAILTEMPLATE").Select(s => s.DefaultTextMax).FirstOrDefault(); //result.Body;
            try
            {
               // if (emailInformation.FullName == null)
                
                    var usr = _context.Users.Where(u => u.Email == emailInformation.EmailAddress).FirstOrDefault();
                
                
                    
                
                if (!isAdmin)
                {
                    if (body.IndexOf("#Content#") > 0)
                    {
                        body = body.Replace("#Content#", result.Body);
                    }
                }
                else
                {
                    body = result.Body;
                    emailInformation.EmailAddress = result.SenderEmail;
                }

                if (body.IndexOf("#User#") > 0)
                {
                    if (emailInformation.FullName != null)
                    {
                        body = body.Replace("#User#", (String.IsNullOrEmpty(emailInformation.FullName) ? emailInformation.UserName : emailInformation.UserName));

                    }
                    else
                    {
                        body = body.Replace("#User#", (String.IsNullOrEmpty(usr.FirstName.Trim()) ? emailInformation.UserName : usr.FirstName));
                    }
                }
                if (body.IndexOf("#LogoLink#") > 0)
                {
                    body = body.Replace("#LogoLink#", System.Configuration.ConfigurationManager.AppSettings["LogoLink"]);
                }


                // System.Configuration.ConfigurationManager.AppSettings["ActivationLink"]


                if (body.IndexOf("#WebLink#") > 0)
                {
                    body = body.Replace("#WebLink#", (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Staging"])) ? System.Configuration.ConfigurationManager.AppSettings["DevActivationLink"] : System.Configuration.ConfigurationManager.AppSettings["ActivationLink"] );
                }
                
                if (body.IndexOf("#ActivationLink#") > 0)
                {
                    body = body.Replace("#ActivationLink#", emailInformation.ActivationLink);
                }
              
                if (body.IndexOf("#TelegramLink#") > 0)
                {
                    body = body.Replace("#TelegramLink#",System.Configuration.ConfigurationManager.AppSettings[""]);
                }
                if (body.IndexOf("#FacbookLink#") > 0)
                {
                    body = body.Replace("#FacbookLink#", System.Configuration.ConfigurationManager.AppSettings[""]);
                }
                if (body.IndexOf("#TwitterLink#") > 0)
                {
                    body = body.Replace("#TwitterLink#", System.Configuration.ConfigurationManager.AppSettings[""]);
                }
               
                if (body.IndexOf("#YoutubeLink#") > 0)
                {
                    body = body.Replace("#YoutubeLink#", System.Configuration.ConfigurationManager.AppSettings[""]);
                }



                SmtpClient smtp = new SmtpClient
                {
                    Host = result.HostName,
                    Port = result.HostPort,
                    EnableSsl = result.EnableSSL,
                   
                    Credentials = new System.Net.NetworkCredential(result.SMTPUsername, result.SMTPPassword),
                };

                MailMessage message = new MailMessage(result.SMTPUsername, emailInformation.EmailAddress, result.Subject, body);
                message.From = new MailAddress(result.SenderEmail, result.SenderName);
                message.IsBodyHtml = true;
                if (!String.IsNullOrEmpty(emailInformation.Attachement) && !String.IsNullOrEmpty(emailInformation.FileName))
                {
                    byte[] bytes = Convert.FromBase64String(emailInformation.Attachement);
                    Stream stream = new MemoryStream(bytes);
                    Attachment attachment = new Attachment(stream, emailInformation.FileName);
                    message.Attachments.Add(attachment);

                }
                smtp.Send(message);
                error = "Success";
            }
            catch (Exception Ex)
            {
                error = Ex.ToString();
                // _logService.writeLog(_env.WebRootPath, Ex.ToString());
            }
            return error;
        }



        public (String result, bool Succeeded, string Error) FileToString(string FilePath)
        {
            try
            {
                string contentRootPath = "";
                String FileFullPath = contentRootPath + FilePath;
                if (File.Exists(FileFullPath))
                {
                    string contents = File.ReadAllText(FilePath);
                    if (!String.IsNullOrEmpty(contents.Trim()))
                    {
                        contents = contents.Replace("\r\n", string.Empty);
                        RegexOptions options = RegexOptions.None;
                        Regex regex = new Regex("[ ]{2,}", options);
                        contents = regex.Replace(contents, " ");
                        return (contents, true, "");
                    }
                    else
                    {
                        return ("", false, "No record found!!!");
                    }
                }
                else
                {
                    return ("", false, "No file found!!!");
                }
            }
            catch (Exception ex)
            {
              
                return ("", false, ex.Message);
            }
        }

        public string Decrypt(string encryptText)
        {
            try
            {
               
                string encryptionkey = System.Configuration.ConfigurationManager.AppSettings["Key"];
                byte[] keybytes = Encoding.ASCII.GetBytes(encryptionkey.Length.ToString());
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                byte[] encryptedData = Convert.FromBase64String(encryptText.Replace(" ", "+").Replace("PLUS", "+"));
                PasswordDeriveBytes pwdbytes = new PasswordDeriveBytes(encryptionkey, keybytes);
                using (ICryptoTransform decryptrans = rijndaelCipher.CreateDecryptor(pwdbytes.GetBytes(32), pwdbytes.GetBytes(16)))
                {
                    using (MemoryStream mstrm = new MemoryStream(encryptedData))
                    {
                        using (CryptoStream cryptstm = new CryptoStream(mstrm, decryptrans, CryptoStreamMode.Read))
                        {
                            byte[] plainText = new byte[encryptedData.Length];
                            int decryptedCount = cryptstm.Read(plainText, 0, plainText.Length);
                            return Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //_logService.writeLog(_env.WebRootPath, ex.Message.ToString() + "Stack - " + ex.ToString());
                return ex.Message;
            }
        }

        public string Encrypt(string inputText)
        {
            try
            {
                string encryptionkey = System.Configuration.ConfigurationManager.AppSettings["Key"];
                byte[] keybytes = Encoding.ASCII.GetBytes(encryptionkey.Length.ToString());
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                byte[] plainText = Encoding.Unicode.GetBytes(inputText);
                PasswordDeriveBytes pwdbytes = new PasswordDeriveBytes(encryptionkey, keybytes);
                using (ICryptoTransform encryptrans = rijndaelCipher.CreateEncryptor(pwdbytes.GetBytes(32), pwdbytes.GetBytes(16)))
                {
                    using (MemoryStream mstrm = new MemoryStream())
                    {
                        using (CryptoStream cryptstm = new CryptoStream(mstrm, encryptrans, CryptoStreamMode.Write))
                        {
                            cryptstm.Write(plainText, 0, plainText.Length);
                            cryptstm.Close();
                            return Convert.ToBase64String(mstrm.ToArray()).Replace("+", "PLUS");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
               // _logService.writeLog(_env.WebRootPath, ex.Message.ToString() + "Stack - " + ex.ToString());
                return ex.Message;
            }
        }


        //public static ClaimsPrincipal GetPrincipal(string token)
        //{
        //    try
        //    {
        //        var tokenHandler = new JwtSecurityTokenHandler();
        //        var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

        //        if (jwtToken == null)
        //            return null;

        //        var symmetricKey = Convert.FromBase64String(Secret);

        //        var validationParameters = new TokenValidationParameters()
        //        { 
        //            RequireExpirationTime = true,
        //            ValidateIssuer = false,
        //            ValidateAudience = false,
        //            IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
        //        };

        //        SecurityToken securityToken;
        //        var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

        //        return principal;
        //    }

        //    catch (Exception)
        //    {
        //        //should write log
        //        return null;
        //    }
        //}



        #endregion



    }
}
