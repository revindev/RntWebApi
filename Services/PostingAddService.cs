﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using RentAndTake.Context;
using RentAndTake.Models;


namespace RentAndTake.Services
{
    public class PostingAddService : IPostingAddService
    {

        #region <variables>                       

        private readonly RnTContext _context;
        private static Random random = new Random();
        #endregion
        public PostingAddService(RnTContext RnTContext)
        {
            _context = RnTContext;
        }

        #region Post adds
        public async Task<(string result, bool Succeeded)> CreateAdd(Product_Form product_Form, ClaimsPrincipal user)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {


                    // int Count = _userManager.Users.Count() + 1;
                    long ticketid = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssf"));
                    var productID = Guid.NewGuid();
                    var id = "PRO-" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

                    var searchtag = product_Form.SearchTags;
                    string search = string.Empty;
                    string startupPath = string.Empty;
                    string dirUrl = "PostedADSImages" + "/" + RemoveSpecialCharacters(product_Form.Owner_Id);
                    string fileUrl = dirUrl + "/" + id;
                    int picCount = 1;
                    string PathCoverPIc =id + "COVER";
                    String path = HttpContext.Current.Server.MapPath("~/ImageStorage" +"/"+ fileUrl); //Path
                    String path1 = "ImageStorage" + "/" + fileUrl; //Path




                    foreach (var item in searchtag)
                    {
                        search = search +" "+ item.ToString();                       
                    }

                    #region UserCreation
                    _context.Products.Add(new Product
                    {


                        PId = productID,
                        Id = id,
                        // ProductNumber = productNumber;
                        Display_Name = product_Form.Display_Name,
                        Cat_Id = product_Form.Cat_Id,
                        Sub_Cat_Id = product_Form.Sub_Cat_Id,

                        Condition = product_Form.Condition,

                        rent_by_hour = (product_Form.rent_by_hour.Trim() == string.Empty ? 0 : Convert.ToDecimal(product_Form.rent_by_hour)),
                        rent_by_day = (product_Form.rent_by_day.Trim() == string.Empty ? 0 : Convert.ToDecimal(product_Form.rent_by_day)),
                        Sale_Amount = (product_Form.Sale_Amount.Trim() == string.Empty ? 0 : Convert.ToDecimal(product_Form.Sale_Amount)),
                        Ship = (product_Form.Ship.Trim() == string.Empty ? 0 : Convert.ToDecimal(product_Form.Ship)),


                        //   rent_by_hour = Convert.ToDecimal( product_Form.rent_by_hour),
                        // rent_by_day = Convert.ToDecimal(product_Form.rent_by_day),
                        //Sale_Amount = Convert.ToDecimal(product_Form.Sale_Amount),
                        Product_Description = product_Form.Product_Description,
                        //  Ship = Convert.ToDecimal(product_Form.Ship),
                        Owner_Id = product_Form.Owner_Id,
                        Status = true,
                        Loc_City_State_Country = product_Form.Loc_City_State_Country,
                        date_entered = DateTime.UtcNow.Date,
                        Date_modified = DateTime.UtcNow.Date,
                        Rent_D_negotiable = product_Form.Rent_D_negotiable,
                        Rent_H_negotiable = product_Form.Rent_H_negotiable,
                        Sale_Negotiable = product_Form.Sale_negotiable,
                        Ad_type = Convert.ToInt32(product_Form.Ad_type),
                        //  Photos = product_Form.Photos
                        // Photos = path+"/"+PathCoverPIc,// path +"/"+ id + "cover",
                        Photos = path1 + "/" + PathCoverPIc,// path +"/"+ id + "cover",
                        
                        SearchTags = search
                        // ,weight=product_Form.Sale_Amount

                    });

                    SaveImage(product_Form.Photos, PathCoverPIc, path);

                    if (product_Form.Images.Count >= 1)
                    {
                        foreach (var image in product_Form.Images)
                        {
                            string filename = id + picCount.ToString();
                          // string filePath = fileUrl +"/"+ filename;
                            string filePath = path1 + "/" + filename;

                            string url = fileUrl;
                            _context.product_Photos.Add(new Product_Photos
                            {
                                PhotoId = Guid.NewGuid(),
                                Pid = productID.ToString(),
                                Photo_file_name = filename,
                                Photo_description = path,
                                Photo = filePath,
                                Display_status = true,


                                Date_entered = DateTime.UtcNow.Date

                            });

                            SaveImage(image.Photo, filename, path);
                            picCount++;
                        }
                    }
                //  SaveImage(product_Form.Photos, id + "COVER", fileUrl);

                    _context.SaveChanges();
                    dbContextTransaction.Commit();

                    var Result = new { value = "Add is Posted Successfully" };
                    return (JsonConvert.SerializeObject(Result), true);
                    #endregion
                }


                catch (Exception Ex)
                {
                    string s = Ex.InnerException.Message;
                    string s1 = Ex.InnerException.ToString();


                    dbContextTransaction.Rollback();

                  //  var response2 = new { value = "Some server error has occurred. Please try again!!" };
                    return (JsonConvert.SerializeObject("Some server error has occurred. Please try again!!"), false);
                }
            }

        }

        public bool SaveImage(string ImgStr, string ImgName, string path)
        {
            //String path = HttpContext.Current.Server.MapPath("~/ImageStorage"+ path1); //Path

            //Check if directory exist
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
            }

            string imageName = ImgName + ".png";

            //set the image path
            string imgPath = Path.Combine(path, imageName);

            byte[] imageBytes = Convert.FromBase64String(ImgStr);

            File.WriteAllBytes(imgPath, imageBytes);

            return true;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_]+", "", RegexOptions.Compiled);
        }

        #endregion


        #region get posts and search 
        public async Task<(string result, bool Succeeded)> GetPost(String CatId, String SubCatId)
        {
            try
            {
                bool subcat = false;
                if(!String.IsNullOrEmpty(SubCatId))
                {
                    if(!String.IsNullOrEmpty(SubCatId.Trim()))
                    {
                        subcat = true;
                    }
                }

                bool cat = false;
                if (!String.IsNullOrEmpty(CatId))
                {
                    if (!String.IsNullOrEmpty(CatId.Trim()))
                    {
                        cat = true;
                    }
                }


                // for the case of all 
                if(!cat)
                {
                    #region all category
                    var result = (from p in _context.Products
                                  where  p.Status == true

                                  select new
                                  {
                             
                                      DisplayName = p.Display_Name,
                                      productId = p.Id,
                                    
                                      rentbyhour = p.rent_by_hour,
                                      rentbyday = p.rent_by_day,
                                      SaleAmount = p.Sale_Amount,
                                      photos= p.Photos+".png",
                                      Ship = p.Ship,
                                      OwnerId = p.Owner_Id,
                                      //photos = p.Photos,
                                      Ad_Type = p.Ad_type,
                                      color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                      LocCity_State_Country = p.Loc_City_State_Country
                                      // RentDnegotiable = p.Rent_D_negotiable,
                                      //  RentHnegotiable = p.Rent_H_negotiable,
                                      // Salenegotiable = p.Sale_Negotiable,
                                      // DisplayName = c.Display_Name,
                                      //   CategoryId = p.Cat_Id,
                                      //   SubCategoryId = p.Sub_Cat_Id,
                                      //   Condition = p.Condition,
                                      //   ProductDescription = p.Product_Description,
                                      //  UID = p.Owner_Id,
                                      //  id = p.Id,

                                      // Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                  });


                    if (result.Count() > 0)
                    {
                        return (JsonConvert.SerializeObject(result), true);
                    }
                    else
                    {

                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }
                    #endregion
                }
                else if (!subcat)
                {
                    #region BY catgeories 
                    var result = (from p in _context.Products
                                  where p.Cat_Id == CatId && p.Status==true

                                  select new
                                  {
                                      DisplayName = p.Display_Name,
                                      productId = p.Id,

                                      rentbyhour = p.rent_by_hour,
                                      rentbyday = p.rent_by_day,
                                      SaleAmount = p.Sale_Amount,
                                      photos =  p.Photos + ".png",
                                      Ship = p.Ship,
                                      OwnerId = p.Owner_Id,
                                      //photos = p.Photos,
                                      Ad_Type = p.Ad_type,
                                      color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                      LocCity_State_Country = p.Loc_City_State_Country,


                                      //UID = p.Owner_Id,
                                      //DisplayName = p.Display_Name,
                                      //productId = p.Id,
                                      //// DisplayName = c.Display_Name,
                                      //CategoryId = p.Cat_Id,
                                      //SubCategoryId = p.Sub_Cat_Id,
                                      //Condition = p.Condition,
                                      //rentbyhour = p.rent_by_hour,
                                      //rentbyday = p.rent_by_day,
                                      //SaleAmount = p.Sale_Amount,
                                      //ProductDescription = p.Product_Description,
                                      //Ship = p.Ship,
                                      //OwnerId = p.Owner_Id,
                                      //id = p.Id,
                                      //LocCity_State_Country = p.Loc_City_State_Country,
                                      //RentDnegotiable = p.Rent_D_negotiable,
                                      //RentHnegotiable = p.Rent_H_negotiable,
                                      //Salenegotiable = p.Sale_Negotiable,
                                      //photos = p.Photos,
                                      //Ad_Type = p.Ad_type,
                                      //color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black")

                                      // Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                  });


                    if (result.Count() > 0)
                    {
                        return (JsonConvert.SerializeObject(result), true);
                    }
                    else
                    {
                       
                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }
                    #endregion
                }
                else
                {
                    #region by categories and subcategories 
                    var result = (from p in _context.Products
                                  where p.Cat_Id == CatId && p.Sub_Cat_Id == SubCatId && p.Status == true
                                  select new
                                  {

                                      DisplayName = p.Display_Name,
                                      productId = p.Id,

                                      rentbyhour = p.rent_by_hour,
                                      rentbyday = p.rent_by_day,
                                      SaleAmount = p.Sale_Amount,
                                      photos =  p.Photos + ".png",
                                      Ship = p.Ship,
                                      OwnerId = p.Owner_Id,
                                      //photos = p.Photos,
                                      Ad_Type = p.Ad_type,
                                      color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                      LocCity_State_Country = p.Loc_City_State_Country,

                                      //UID = p.Owner_Id,
                                      //DisplayName = p.Display_Name,
                                      //productId = p.ProductNumber,
                                      //// DisplayName = c.Display_Name,
                                      //CategoryId = p.Cat_Id,
                                      //SubCategoryId = p.Sub_Cat_Id,
                                      //Condition = p.Condition,
                                      //rentbyhour = p.rent_by_hour,
                                      //rentbyday = p.rent_by_day,
                                      //SaleAmount = p.Sale_Amount,
                                      //ProductDescription = p.Product_Description,
                                      //Ship = p.Ship,
                                      //id = p.Id,
                                      //OwnerId = p.Owner_Id,
                                      //LocCity_State_Country = p.Loc_City_State_Country,
                                      //RentDnegotiable = p.Rent_D_negotiable,
                                      //RentHnegotiable = p.Rent_H_negotiable,
                                      //Salenegotiable = p.Sale_Negotiable,

                                      //photos = p.Photos,
                                      //Ad_Type = p.Ad_type,
                                      //color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),



                                      // Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                  });


                    if (result.Count() > 0)
                    {
                        return (JsonConvert.SerializeObject(result), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return (JsonConvert.SerializeObject("oops Something went wrong!!"), false);
            }

        }

        public (string result, bool Succeeded) GetPostByFilters(ProductFilterNew productFilterNew)
        {
            try
            {
                
                
                var result = (from p in _context.Products
                              where  p.Status == true

                              select new
                              {
                                  //new 
                                  DisplayName = p.Display_Name,
                                  productId = p.Id,

                                  rentbyhour = p.rent_by_hour,
                                  rentbyday = p.rent_by_day,
                                  SaleAmount = p.Sale_Amount,
                                  photos = p.Photos + ".png",
                                  Ship = p.Ship,
                                  OwnerId = p.Owner_Id,
                                  //photos = p.Photos,
                                  Ad_Type = p.Ad_type,
                                  color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                  LocCity_State_Country = p.Loc_City_State_Country,
                                  //end

                                  ////  UID = p.Owner_Id,
                                  //  DisplayName = p.Display_Name,
                                  // // productId = p.ProductNumber,
                                  //  id = p.Id,
                                    CategoryId = p.Cat_Id,
                                   SubCategoryId = p.Sub_Cat_Id,
                                    Condition = p.Condition,
                                  //  rentbyhour = p.rent_by_hour,
                                  //  rentbyday = p.rent_by_day,
                                  //  SaleAmount = p.Sale_Amount,
                                  // // ProductDescription = p.Product_Description,
                                  //  Ship = p.Ship,
                                  ////  OwnerId = p.Owner_Id,
                                  //  LocCity_State_Country = p.Loc_City_State_Country,
                                  // // RentDnegotiable = p.Rent_D_negotiable,
                                  // // RentHnegotiable = p.Rent_H_negotiable,
                                  ////  Salenegotiable = p.Sale_Negotiable,
                                  //  photos=p.Photos,
                                    Adtype=p.Ad_type,
                                  //  color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black")

                                  //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                              }).ToList();


                if (!string.IsNullOrEmpty(productFilterNew.Cat_Id))
                {
                    result = result.Where(t => t.CategoryId == productFilterNew.Cat_Id).ToList();
                }
                if (!string.IsNullOrEmpty(productFilterNew.Sub_Cat_Id))
                {
                    result = result.Where(t => t.SubCategoryId == productFilterNew.Sub_Cat_Id).ToList();
                }
                if (productFilterNew.AdTYpe.Count>0)
                {
                    List<int> str = productFilterNew.AdTYpe;
                    str.Add(3);
                    result= result.Where(x => (str.Contains(x.Adtype))).ToList();
                }
                if (productFilterNew.Condition.Count > 0)
                {
                    List<string> str = productFilterNew.Condition;
                   result= result.Where(x => (str.Contains(x.Condition.ToString()))).ToList();
                }               
                if (productFilterNew.MinPrice!=0 && productFilterNew.MaxPrice!=0)
                {
                    result = result.Where(t => (t.rentbyday >= productFilterNew.MinPrice && t.rentbyday<= productFilterNew.MaxPrice) ||
                    (t.rentbyhour >= productFilterNew.MinPrice && t.rentbyhour <= productFilterNew.MaxPrice) ||
                    (t.SaleAmount >= productFilterNew.MinPrice && t.SaleAmount <= productFilterNew.MaxPrice)
                    ).ToList();
                  //  result = result.Where(t => t.rentbyday >= productFilter.MinPrice && t.rentbyday <= productFilter.MaxPrice);
                  
                }


                if (result.Count() > 0)
                {
                    return (JsonConvert.SerializeObject(result), true);
                }
                else
                {
                    return (JsonConvert.SerializeObject("No record found!!"), false);
                }

              //  return (JsonConvert.SerializeObject(result), true);

            }
            catch (Exception ex)
            {
                //_logService.writeLog(_env.WebRootPath, ex.ToString());
               
                return (JsonConvert.SerializeObject(""), true);
            }

        }

        public async Task<(string result, bool Succeeded)> GetPostSearch(String SearchText)//string CatId, String SubCatId)
        {
            try
            {
                //if (!string.IsNullOrEmpty(CatId))                    
                {
                    //var punctuation = SearchText.Where(Char.IsPunctuation).Distinct().ToArray();
                    // var words = SearchText.Split().Select(x => x.Trim(punctuation)).ToList();

                    var result = (from p in _context.Products
                                  where p.Status == true
                                  && p.SearchTags.Contains(SearchText)
                                  select new
                                  {



                                      //new 
                                      DisplayName = p.Display_Name,
                                      productId = p.Id,

                                      rentbyhour = p.rent_by_hour,
                                      rentbyday = p.rent_by_day,
                                      SaleAmount = p.Sale_Amount,
                                      photos = p.Photos + ".png",
                                      Ship = p.Ship,
                                      OwnerId = p.Owner_Id,
                                      //photos = p.Photos,
                                      Ad_Type = p.Ad_type,
                                      color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                      LocCity_State_Country = p.Loc_City_State_Country,
                                  //end
                                  // UID = p.Owner_Id,
                                  // DisplayName = p.Display_Name,
                                  //// productId = p.ProductNumber,
                                  // productId = p.Id,
                                  // id =p.Id,
                                  // CategoryId = p.Cat_Id,
                                  // SubCategoryId = p.Sub_Cat_Id,
                                  // Condition = p.Condition,
                                  // rentbyhour = p.rent_by_hour,
                                  // rentbyday = p.rent_by_day,
                                  // SaleAmount = p.Sale_Amount,
                                  // ProductDescription = p.Product_Description,
                                  // Ship = p.Ship,
                                  // OwnerId = p.Owner_Id,
                                  // LocCity_State_Country = p.Loc_City_State_Country,
                                  // RentDnegotiable = p.Rent_D_negotiable,
                                  // RentHnegotiable = p.Rent_H_negotiable,
                                  // Salenegotiable = p.Sale_Negotiable,
                                  // photos = p.Photos,
                                  // Adtype = p.Ad_type,
                                  // color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black")
                                  //,
                                      Searchtxt = p.SearchTags
                                      //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                  }).ToList();


                    if (result.Count() > 0)
                    {

                        return (JsonConvert.SerializeObject(result), true);

                        //if (!string.IsNullOrEmpty(CatId))
                        //{
                        //    result = result.Where(t => t.CategoryId == (CatId)).ToList();
                        //}
                        //if (!string.IsNullOrEmpty(SubCatId))
                        //{
                        //    result = result.Where(t => t.SubCategoryId == (SubCatId)).ToList();
                        //}

                        ////if (!string.IsNullOrEmpty(SearchText))
                        ////{
                        ////  //  List<string> str = SearchText.Split(',').ToList();

                        ////    List<string> str = SearchText.Split(' ').ToList();
                        ////    result = result.Where(x => (str.Contains(x.Searchtxt))).ToList();

                        ////}

                        //if (!string.IsNullOrEmpty(SearchText))
                        //{
                        //    //  List<string> str = SearchText.Split(',').ToList();


                        //   // result = result.Where(x => (SearchText.Contains(x.Searchtxt))).ToList();
                        //    result=(result).Where(row => row.Searchtxt.Contains(SearchText)).ToList();
                        //}

                        //if (!string.IsNullOrEmpty(SearchText))
                        //{
                        //    //  List<string> str = SearchText.Split(',').ToList();

                        //    List<string> str = SearchText.Split(' ').ToList();

                        //    foreach (var colour in str)
                        //    {
                        //        if (colour != null)

                        //            result = result.Where(oh => oh.Searchtxt.Contains(colour)).ToList();

                        //    }
                        //}



                    }
                    else
                    {
                        // result = (JsonConvert.SerializeObject("No record found!!"), false);
                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }




                }
            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return (JsonConvert.SerializeObject("oops Something went wrong!!"), false);
            }

        }
      


        public async Task<(string result, bool Succeeded)> GetPostSearchWithcatAndSubCat(String SearchText, string CatId, String SubCatId)
        {
            try
            {
                //if (!string.IsNullOrEmpty(CatId))                    

                //var punctuation = SearchText.Where(Char.IsPunctuation).Distinct().ToArray();
                // var words = SearchText.Split().Select(x => x.Trim(punctuation)).ToList();

                #region code to find the cases
                bool Checksubcat = false;
                    if (!String.IsNullOrEmpty(SubCatId))
                    {
                        if (!String.IsNullOrEmpty(SubCatId.Trim()))
                        {
                            Checksubcat = true;
                        }
                    }

                    bool Checkcat = false;
                    if (!String.IsNullOrEmpty(CatId))
                    {
                        if (!String.IsNullOrEmpty(CatId.Trim()))
                        {
                            Checkcat = true;
                        }
                    }

                bool CheckText = false;
                if (!String.IsNullOrEmpty(SearchText))
                {
                    if (!String.IsNullOrEmpty(SearchText.Trim()))
                    {
                        CheckText = true;
                    }
                }
                #endregion
                //cases of search 

                if (!CheckText && !Checkcat && !CheckText)
                {
                    #region case:1 search with no value or empty search without text : show all
                    var result1 = (from p in _context.Products
                                   where p.Status == true                                
                                   select new
                                   {



                                       //new 
                                       DisplayName = p.Display_Name,
                                       productId = p.Id,

                                       rentbyhour = p.rent_by_hour,
                                       rentbyday = p.rent_by_day,
                                       SaleAmount = p.Sale_Amount,
                                       photos = p.Photos + ".png",
                                       Ship = p.Ship,
                                       OwnerId = p.Owner_Id,
                                       //photos = p.Photos,
                                       Ad_Type = p.Ad_type,
                                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                       LocCity_State_Country = p.Loc_City_State_Country,

                                       Searchtxt = p.SearchTags,
                                       CatId = p.Cat_Id,
                                       SubCatId = p.Sub_Cat_Id,
                                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                   }).ToList();

                    if (result1.Count() > 0)
                    {
                        
                        return (JsonConvert.SerializeObject(result1), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record Found"), false);
                                              

                    }
                    #endregion
                }
                else if (Checkcat && !CheckText && !Checksubcat)
                {
                    #region case:2 search with only cat without text 

                    var result1 = (from p in _context.Products
                                   where p.Status == true && p.Cat_Id==CatId
                                   select new
                                   {



                                       //new 
                                       DisplayName = p.Display_Name,
                                       productId = p.Id,

                                       rentbyhour = p.rent_by_hour,
                                       rentbyday = p.rent_by_day,
                                       SaleAmount = p.Sale_Amount,
                                       photos = p.Photos + ".png",
                                       Ship = p.Ship,
                                       OwnerId = p.Owner_Id,
                                       //photos = p.Photos,
                                       Ad_Type = p.Ad_type,
                                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                       LocCity_State_Country = p.Loc_City_State_Country,

                                       Searchtxt = p.SearchTags,
                                       CatId = p.Cat_Id,
                                       SubCatId = p.Sub_Cat_Id,
                                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                   }).ToList();

                    if (result1.Count() > 0)
                    {

                        return (JsonConvert.SerializeObject(result1), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record Found"), false);


                    }
                  


                    #endregion

                }
                else if (Checkcat && Checksubcat && !CheckText)
                {
                    #region case:3 search with only cat and subcat without text 


                    var result1 = (from p in _context.Products
                                   where p.Status == true && p.Cat_Id == CatId && p.Sub_Cat_Id==SubCatId
                                   select new
                                   {



                                       //new 
                                       DisplayName = p.Display_Name,
                                       productId = p.Id,

                                       rentbyhour = p.rent_by_hour,
                                       rentbyday = p.rent_by_day,
                                       SaleAmount = p.Sale_Amount,
                                       photos = p.Photos + ".png",
                                       Ship = p.Ship,
                                       OwnerId = p.Owner_Id,
                                       //photos = p.Photos,
                                       Ad_Type = p.Ad_type,
                                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                       LocCity_State_Country = p.Loc_City_State_Country,

                                       Searchtxt = p.SearchTags,
                                       CatId = p.Cat_Id,
                                       SubCatId = p.Sub_Cat_Id,
                                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                   }).ToList();

                    if (result1.Count() > 0)
                    {

                        return (JsonConvert.SerializeObject(result1), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record Found"), false);
                        
                    }
                    

                    #endregion
                }

                else if (Checkcat && Checksubcat && CheckText)
                {
                    #region case:4 search with  cat , subcat and text 

                    var result1 = (from p in _context.Products
                                   where p.Status == true && p.Cat_Id == CatId && p.Sub_Cat_Id == SubCatId

                                   select new
                                   {



                                       //new 
                                       DisplayName = p.Display_Name,
                                       productId = p.Id,

                                       rentbyhour = p.rent_by_hour,
                                       rentbyday = p.rent_by_day,
                                       SaleAmount = p.Sale_Amount,
                                       photos = p.Photos + ".png",
                                       Ship = p.Ship,
                                       OwnerId = p.Owner_Id,
                                       //photos = p.Photos,
                                       Ad_Type = p.Ad_type,
                                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                       LocCity_State_Country = p.Loc_City_State_Country,

                                       Searchtxt = p.SearchTags,
                                       CatId = p.Cat_Id,
                                       SubCatId = p.Sub_Cat_Id,
                                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                   }).ToList();

                    if (result1.Count() > 0)
                    {

                        if (!String.IsNullOrEmpty(SearchText))
                        {
                            result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                        }
                        return (JsonConvert.SerializeObject(result1), true);

                        //result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                        ////  return (JsonConvert.SerializeObject("No record Found"), false);
                        //return (JsonConvert.SerializeObject(result1), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record Found"), false);

                    }

                    #endregion
                }
                else if (CheckText && !Checkcat && !Checksubcat)
                {
                    #region case:5 search with only text 




                    var result1 = (from p in _context.Products
                                   where p.Status == true && p.SearchTags.Contains(SearchText)
                                   select new
                                   {



                                       //new 
                                       DisplayName = p.Display_Name,
                                       productId = p.Id,

                                       rentbyhour = p.rent_by_hour,
                                       rentbyday = p.rent_by_day,
                                       SaleAmount = p.Sale_Amount,
                                       photos = p.Photos + ".png",
                                       Ship = p.Ship,
                                       OwnerId = p.Owner_Id,
                                       //photos = p.Photos,
                                       Ad_Type = p.Ad_type,
                                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                       LocCity_State_Country = p.Loc_City_State_Country,

                                       Searchtxt = p.SearchTags,
                                       CatId = p.Cat_Id,
                                       SubCatId = p.Sub_Cat_Id,
                                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                   }).ToList();

                    if (result1.Count() > 0)
                    {

                        return (JsonConvert.SerializeObject(result1), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record Found"), false);

                    }


                   


                    #endregion
                }
                else if (CheckText && Checkcat && !Checksubcat)
                {
                    #region case:6 search with  cat and text 

                    var result1 = (from p in _context.Products
                                   where p.Status == true && p.Cat_Id == CatId 

                                   select new
                                   {



                                       //new 
                                       DisplayName = p.Display_Name,
                                       productId = p.Id,

                                       rentbyhour = p.rent_by_hour,
                                       rentbyday = p.rent_by_day,
                                       SaleAmount = p.Sale_Amount,
                                       photos = p.Photos + ".png",
                                       Ship = p.Ship,
                                       OwnerId = p.Owner_Id,
                                       //photos = p.Photos,
                                       Ad_Type = p.Ad_type,
                                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                       LocCity_State_Country = p.Loc_City_State_Country,

                                       Searchtxt = p.SearchTags,
                                       CatId = p.Cat_Id,
                                       SubCatId = p.Sub_Cat_Id,
                                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                   }).ToList();

                    if (result1.Count() > 0)
                    {

                        if (!String.IsNullOrEmpty(SearchText))
                        {
                            result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                        }
                        return (JsonConvert.SerializeObject(result1), true);

                        //result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                        ////  return (JsonConvert.SerializeObject("No record Found"), false);
                        //return (JsonConvert.SerializeObject(result1), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record Found"), false);

                    }

                    #endregion

                }
                else
                {
                    #region  case:7 default  : show all
                    var result1 = (from p in _context.Products
                                   where p.Status == true
                                   select new
                                   {



                                       //new 
                                       DisplayName = p.Display_Name,
                                       productId = p.Id,

                                       rentbyhour = p.rent_by_hour,
                                       rentbyday = p.rent_by_day,
                                       SaleAmount = p.Sale_Amount,
                                       photos = p.Photos + ".png",
                                       Ship = p.Ship,
                                       OwnerId = p.Owner_Id,
                                       //photos = p.Photos,
                                       Ad_Type = p.Ad_type,
                                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                                       LocCity_State_Country = p.Loc_City_State_Country,

                                       Searchtxt = p.SearchTags,
                                       CatId = p.Cat_Id,
                                       SubCatId = p.Sub_Cat_Id,
                                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                                   }).ToList();

                    if (result1.Count() > 0)
                    {

                        return (JsonConvert.SerializeObject(result1), true);
                    }
                    else
                    {
                        return (JsonConvert.SerializeObject("No record Found"), false);


                    }
                    #endregion

                }

                //if (Checkcat && !Checksubcat)
                //{
                //    //if (String.IsNullOrEmpty(SearchText))
                //    //{
                //    //    var result1 = (from p in _context.Products
                //    //                   where p.Status == true// && p.Cat_Id == CatId
                //    //                    && p.SearchTags.Contains(SearchText)
                //    //                   select new
                //    //                   {



                //    //                       //new 
                //    //                       DisplayName = p.Display_Name,
                //    //                       productId = p.Id,

                //    //                       rentbyhour = p.rent_by_hour,
                //    //                       rentbyday = p.rent_by_day,
                //    //                       SaleAmount = p.Sale_Amount,
                //    //                       photos = p.Photos + ".png",
                //    //                       Ship = p.Ship,
                //    //                       OwnerId = p.Owner_Id,
                //    //                       //photos = p.Photos,
                //    //                       Ad_Type = p.Ad_type,
                //    //                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                //    //                       LocCity_State_Country = p.Loc_City_State_Country,

                //    //                       Searchtxt = p.SearchTags,
                //    //                       CatId = p.Cat_Id,
                //    //                       SubCatId = p.Sub_Cat_Id,
                //    //                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                //    //                   }).ToList();

                //    //    if (result1.Count() > 0)
                //    //    {
                //    //        result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                //    //        return (JsonConvert.SerializeObject(result1), true);
                //    //    }
                //    //    else
                //    //    {
                //    //        return (JsonConvert.SerializeObject("No record Found"), false);

                //    //    }
                //    //}
                //    //else
                //    {
                //        var result1 = (from p in _context.Products
                //                       where p.Status == true && p.Cat_Id == CatId
                //                       // && p.SearchTags.Contains(SearchText)
                //                       select new
                //                       {



                //                           //new 
                //                           DisplayName = p.Display_Name,
                //                           productId = p.Id,

                //                           rentbyhour = p.rent_by_hour,
                //                           rentbyday = p.rent_by_day,
                //                           SaleAmount = p.Sale_Amount,
                //                           photos = p.Photos + ".png",
                //                           Ship = p.Ship,
                //                           OwnerId = p.Owner_Id,
                //                           //photos = p.Photos,
                //                           Ad_Type = p.Ad_type,
                //                           color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                //                           LocCity_State_Country = p.Loc_City_State_Country,

                //                           Searchtxt = p.SearchTags,
                //                           CatId = p.Cat_Id,
                //                           SubCatId = p.Sub_Cat_Id,
                //                           //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                //                       }).ToList();

                //        if (result1.Count() > 0)
                //        {
                //            if (!String.IsNullOrEmpty(SearchText))
                //            {
                //                result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                //            }
                //            return (JsonConvert.SerializeObject(result1), true);
                //        }
                //        else
                //        {
                //            return (JsonConvert.SerializeObject("No record Found"), false);

                //        }
                //    }

                //}
                //else if (Checksubcat)
                //{
                //    var result1 = (from p in _context.Products
                //                   where p.Status == true && p.Cat_Id == CatId && p.Sub_Cat_Id == SubCatId

                //                   select new
                //                   {



                //                       //new 
                //                       DisplayName = p.Display_Name,
                //                       productId = p.Id,

                //                       rentbyhour = p.rent_by_hour,
                //                       rentbyday = p.rent_by_day,
                //                       SaleAmount = p.Sale_Amount,
                //                       photos = p.Photos + ".png",
                //                       Ship = p.Ship,
                //                       OwnerId = p.Owner_Id,
                //                       //photos = p.Photos,
                //                       Ad_Type = p.Ad_type,
                //                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                //                       LocCity_State_Country = p.Loc_City_State_Country,

                //                       Searchtxt = p.SearchTags,
                //                       CatId = p.Cat_Id,
                //                       SubCatId = p.Sub_Cat_Id,
                //                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                //                   }).ToList();

                //    if (result1.Count() > 0)
                //    {

                //        if (!String.IsNullOrEmpty(SearchText))
                //        {
                //            result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                //        }
                //        return (JsonConvert.SerializeObject(result1), true);

                //        //result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                //        ////  return (JsonConvert.SerializeObject("No record Found"), false);
                //        //return (JsonConvert.SerializeObject(result1), true);
                //    }
                //    else
                //    {
                //        return (JsonConvert.SerializeObject("No record Found"), false);

                //    }
                //}
                //else if (!Checkcat && !Checksubcat)

                //{


                //    var result1 = (from p in _context.Products
                //                   where p.Status == true // && p.Cat_Id == CatId && p.Sub_Cat_Id==SubCatId
                //                   && p.SearchTags.Contains(SearchText)
                //                   select new
                //                   {



                //                       //new 
                //                       DisplayName = p.Display_Name,
                //                       productId = p.Id,

                //                       rentbyhour = p.rent_by_hour,
                //                       rentbyday = p.rent_by_day,
                //                       SaleAmount = p.Sale_Amount,
                //                       photos = p.Photos + ".png",
                //                       Ship = p.Ship,
                //                       OwnerId = p.Owner_Id,
                //                       //photos = p.Photos,
                //                       Ad_Type = p.Ad_type,
                //                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                //                       LocCity_State_Country = p.Loc_City_State_Country,

                //                       Searchtxt = p.SearchTags,
                //                       CatId = p.Cat_Id,
                //                       SubCatId = p.Sub_Cat_Id,
                //                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                //                   }).ToList();

                //    if (result1.Count() > 0)
                //    {
                //        //result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                //        return (JsonConvert.SerializeObject(result1), true);
                //    }
                //    else
                //    {
                //        return (JsonConvert.SerializeObject("No record Found"), false);

                //    }

                //}
                //else
                //{
                //    var result1 = (from p in _context.Products
                //                   where p.Status == true // && p.Cat_Id == CatId && p.Sub_Cat_Id==SubCatId
                //                  // && p.SearchTags.Contains(SearchText)
                //                   select new
                //                   {



                //                       //new 
                //                       DisplayName = p.Display_Name,
                //                       productId = p.Id,

                //                       rentbyhour = p.rent_by_hour,
                //                       rentbyday = p.rent_by_day,
                //                       SaleAmount = p.Sale_Amount,
                //                       photos = p.Photos + ".png",
                //                       Ship = p.Ship,
                //                       OwnerId = p.Owner_Id,
                //                       //photos = p.Photos,
                //                       Ad_Type = p.Ad_type,
                //                       color = (p.Ad_type == 1 ? "green" : p.Ad_type == 2 ? "red" : p.Ad_type == 3 ? "blue" : "black"),
                //                       LocCity_State_Country = p.Loc_City_State_Country,

                //                       Searchtxt = p.SearchTags,
                //                       CatId = p.Cat_Id,
                //                       SubCatId = p.Sub_Cat_Id,
                //                       //Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo).ToList()
                //                   }).ToList();

                //    if (result1.Count() > 0)
                //    {
                //        //result1 = result1.Where(t => t.Searchtxt.Contains(SearchText)).ToList();
                //        return (JsonConvert.SerializeObject(result1), true);
                //    }
                //    else
                //    {
                //        return (JsonConvert.SerializeObject("No record Found"), false);

                //    }
                //}
                
                    
                   




                
            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return (JsonConvert.SerializeObject("oops Something went wrong!!"), false);
            }

        }
       

       

        public async Task<(string result, bool Succeeded)> GetPostByProductId(String Pid)
        {
            try
            {

                if (!String.IsNullOrEmpty(Pid))
                {

                    //(DateTime.UtcNow.Date - StartDate).TotalDays
                    //var record = dc.sometabel.Where( u => u.username.Equals("someusername")).SingleOrDefault()
                    var result = (from p in _context.Products
                                  where p.Id == Pid && p.Status == true
                                  
                    select new
                                  {


                                      UID = p.Owner_Id,
                                      DisplayName = p.Display_Name,
                                      productId = p.Id,
                                      // DisplayName = c.Display_Name,
                                      CategoryId = p.Cat_Id,
                                      SubCategoryId = p.Sub_Cat_Id,
                                      Condition = p.Condition,
                                      rentbyhour = p.rent_by_hour,
                                      rentbyday = p.rent_by_day,
                                      SaleAmount = p.Sale_Amount,
                                      ProductDescription = p.Product_Description,
                                      Ship = p.Ship,
                                      OwnerId = p.Owner_Id,
                                      id = p.Id,
                                      LocCity_State_Country = p.Loc_City_State_Country,
                                      RentDnegotiable = p.Rent_D_negotiable,
                                      RentHnegotiable = p.Rent_H_negotiable,
                                      Salenegotiable = p.Sale_Negotiable,
                                      photos = p.Photos+".png",
                                      Ad_Type = p.Ad_type,                    
                       
                        dayago = DbFunctions.DiffDays(p.date_entered, DateTime.UtcNow), //(DateTime.UtcNow - p.date_entered).TotalDays,
                                      Conditionlabel = (p.Condition == "1" ? "Brand New" : p.Condition == "2" ? "Used - Like New" : p.Condition == "3" ? "Used - < 6m" : p.Condition == "4" ? "Used - 6m-1y" : p.Condition == "5" ? "Used - 1y-2y" : p.Condition == "6" ? "Used - > 2y" : "Used - > 2y"),

                                      Images = _context.product_Photos.Where(y => y.Pid == p.PId.ToString()).Select(y => y.Photo+".png").ToList()
                                  });


                    if (result.Count() > 0)
                    {
                        return (JsonConvert.SerializeObject(result), true);
                    }
                    else
                    {

                        return (JsonConvert.SerializeObject("No record found!!"), false);
                    }
                }
                else
                {
                    return (JsonConvert.SerializeObject("Product Id Not Exist!!"), false);
                }
               
            }
            catch (Exception ex)
            {
                // _logService.writeLog(_env.WebRootPath, ex.ToString());
                return (JsonConvert.SerializeObject("oops Something went wrong!!"), false);
            }

        }

        #endregion
        public string ConvertToDayHourMinSec(DateTime firstTime, DateTime secondTime)
        {
            var timeTaken = new TimeSpan(firstTime.Ticks - secondTime.Ticks);
            var days = timeTaken.ToString("%d");
            var hms = timeTaken.ToString(@"hh\:mm\:ss");
            return days + " days, " + hms;
        }

    }
}