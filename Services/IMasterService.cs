﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using RentAndTake.Models;

namespace RentAndTake.Services
{
    public interface IMasterService
    {
        //Task<(string result, bool Succeeded)> GetAllCategory( ClaimsPrincipal user);
        // Task<(string result, bool Succeeded)> GetSubCategory(int Cid,ClaimsPrincipal user);
        // (JsonResult result, bool Succeeded, string Error) GetAllCategory(ClaimsPrincipal user);
        //  (string result, bool Succeeded) GetAllCategory(ClaimsPrincipal user);

        Task<(string result, bool Succeeded)> GetAllCategory(ClaimsPrincipal user);
        Task<(string result, bool Succeeded)> GetSubCategory(string cid, ClaimsPrincipal user);

        Task<(string result, bool Succeeded)> AddCategory(CategoryEntryForm categoryEntryForm, ClaimsPrincipal user);
        Task<(string result, bool Succeeded)> AddSubCategory(SubCategoryEntryForm subCategoryEntryForm, ClaimsPrincipal user);
        Task<(string result, bool Succeeded)> GetNavBar();
    }


}