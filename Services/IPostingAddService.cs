﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using RentAndTake.Models;

namespace RentAndTake.Services
{
    public interface IPostingAddService
    {
        Task<(string result, bool Succeeded)> CreateAdd(Product_Form product_Form , ClaimsPrincipal user);

        Task<(string result, bool Succeeded)> GetPost(String CatId, String SubCatId);

        Task<(string result, bool Succeeded)> GetPostByProductId(String Pid);

        (string result, bool Succeeded) GetPostByFilters(ProductFilterNew productFilterNew);
        //  Task<(string result, bool Succeeded)> GetPostSearch(String SearchText, string CatId,String SubCatId);
        Task<(string result, bool Succeeded)> GetPostSearch(String SearchText);
        Task<(string result, bool Succeeded)> GetPostSearchWithcatAndSubCat(String SearchText, string CatId, String SubCatId);

        // GetPostSearchWithcatAndSubCat(String SearchText, string CatId, String SubCatId)







    }
}
